﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using Facebook.CoreKit;
using Newtonsoft.Json.Linq;
using CoreLocation;
using RestSharp;
using System.Json;
using CoreBluetooth;
using CoreFoundation;
using ObjCRuntime;
using AudioToolbox;
using System.Threading.Tasks;

namespace RoccoGiocattoli
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        CLLocationManager locationmanager;
        NSUuid beaconUUID;
        CLBeaconRegion beaconRegion;
        const string beaconId = "BlueBeacon";
        const string uuid = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";
        Dictionary<string, bool> flagBeacon = new Dictionary<string, bool>();
        public Dictionary<string, Beacon> ListBeacon = new Dictionary<string, Beacon>();
        public List<Messaggio> ListMessaggi = new List<Messaggio>();
        public int i = 0, z = 0 , porta = 81;
        public List<string> BeaconNetworks = new List<string>();
        bool blocco = false;
        bool Background = false;

        public Dictionary<int, XmlObject> XmlObjectDictionary = new Dictionary<int, XmlObject>();
        public int totalValue;

        public string PushToken;

        public string TokenN4U = "";
        public bool flagTokenN4U = true;
        public bool Scan = false;

        public bool BluetoothOff = false;
        public bool BluetoothAlert = false;

        // class-level declarations
        public override UIWindow Window
        {
            get;
            set;
        }

        public static AppDelegate Instance { get; private set; }

        public override void FinishedLaunching(UIApplication application)
        {
            Console.WriteLine(" Senza option");
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }

            MyCentralManagerDelegate cmd = new MyCentralManagerDelegate();
            CBCentralManager _manager = new CBCentralManager(cmd, DispatchQueue.CurrentQueue);
            //_manager.Init();

            startBeacon();


            AppDelegate.Instance = this;
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            //Get current device token
            var DeviceToken = deviceToken.Description;
            Console.WriteLine("PUSH TOKEN1:" + DeviceToken);
            if (!string.IsNullOrWhiteSpace(DeviceToken))
            {
                DeviceToken = DeviceToken.Replace("<", "").Replace(">", "").Replace(" ", "");
            }

            Console.WriteLine("PUSH TOKEN2:" + DeviceToken);
            AppDelegate.Instance.PushToken = DeviceToken;



            //Save new device token 
            NSUserDefaults.StandardUserDefaults.SetString(DeviceToken, "PushDeviceToken");
        }


        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            //This method gets called whenever the app is already running and receives a push notification
            // YOU MUST HANDLE the notifications in this case.  Apple assumes if the app is running, it takes care of everything
            // this includes setting the badge, playing a sound, etc.
            
            Console.WriteLine("remote"+ application.ApplicationState.ToString());
            if(application.ApplicationState == UIApplicationState.Inactive || application.ApplicationState == UIApplicationState.Background)
            {
                if (Background == false)
                {
                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                    if (userInfo != null)
                    {
                        string title = string.Empty;
                        string description = string.Empty;
                        string image_url = string.Empty;
                        string content_url = string.Empty;
                        string _key = string.Empty;


                        if (userInfo.ContainsKey(new NSString("title")))
                            title = (userInfo[new NSString("title")] as NSString).ToString();
                        if (userInfo.ContainsKey(new NSString("description")))
                            description = (userInfo[new NSString("description")] as NSString).ToString();
                        if (userInfo.ContainsKey(new NSString("image_url")))
                        {
                            string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/roccogiocattoli/";
                            string urlimg2 = (userInfo[new NSString("image_url")] as NSString).ToString();
                            string key2 = "";
                            if (urlimg2.Contains("image"))
                            {
                                key2 = urlimg2.Remove(0, 7);
                                Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                            }
                            else
                            {
                                key2 = "null";
                                baseurl = "";
                                Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                            }
                            image_url = baseurl + key2;
                            Console.WriteLine(baseurl + key2);
                        }
                        if (userInfo.ContainsKey(new NSString("content_url")))
                            content_url = (userInfo[new NSString("content_url")] as NSString).ToString();
                        if (userInfo.ContainsKey(new NSString("_key")))
                            _key = (userInfo[new NSString("_key")] as NSString).ToString();

                        Console.WriteLine("object:" + title + "|" + description + "|" + image_url + "|" + content_url + "|" + _key);

                        NSUserDefaults.StandardUserDefaults.SetString(image_url, "ImageNotification");
                        NSUserDefaults.StandardUserDefaults.SetString(description, "DescriptionNotification");
                        NSUserDefaults.StandardUserDefaults.SetString(_key, "KeyNotification");
                        NSUserDefaults.StandardUserDefaults.SetBool(true, "isPushNotification");
                    }
                }
                else
                {
                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                    string title = string.Empty;
                    string description = string.Empty;
                    string image_url = string.Empty;
                    string content_url = string.Empty;
                    string _key = string.Empty;


                    if (userInfo.ContainsKey(new NSString("title")))
                        title = (userInfo[new NSString("title")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("description")))
                        description = (userInfo[new NSString("description")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("image_url")))
                    {
                        string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/roccogiocattoli/";
                        string urlimg2 = (userInfo[new NSString("image_url")] as NSString).ToString();
                        string key2 = "";
                        if (urlimg2.Contains("image"))
                        {
                            key2 = urlimg2.Remove(0, 7);
                            Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                        }
                        else
                        {
                            key2 = "null";
                            baseurl = "";
                            Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                        }
                        image_url = baseurl + key2;
                        Console.WriteLine(baseurl + key2);
                    }
                    if (userInfo.ContainsKey(new NSString("content_url")))
                        content_url = (userInfo[new NSString("content_url")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("_key")))
                        _key = (userInfo[new NSString("_key")] as NSString).ToString();

                    NSUserDefaults.StandardUserDefaults.SetString(image_url, "ImageNotification");
                    NSUserDefaults.StandardUserDefaults.SetString(description, "DescriptionNotification");
                    NSUserDefaults.StandardUserDefaults.SetString(_key, "KeyNotification");
                    NSUserDefaults.StandardUserDefaults.SetBool(true, "isPushNotification");

                    Console.WriteLine("object:" + title + "|" + description + "|" + image_url + "|" + content_url + "|" + _key);

                    UIStoryboard storyboard = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                    }
                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    TopController().PresentModalViewController(lp, true);
                }
  
            }
            if (application.ApplicationState == UIApplicationState.Active)
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                string title = string.Empty;
                string description = string.Empty;
                string image_url = string.Empty;
                string content_url = string.Empty;
                string _key = string.Empty;


                if (userInfo.ContainsKey(new NSString("title")))
                    title = (userInfo[new NSString("title")] as NSString).ToString();
                if (userInfo.ContainsKey(new NSString("description")))
                    description = (userInfo[new NSString("description")] as NSString).ToString();
                if (userInfo.ContainsKey(new NSString("image_url")))
                {
                    string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/roccogiocattoli/";
                    string urlimg2 = (userInfo[new NSString("image_url")] as NSString).ToString();
                    string key2 = "";
                    if (urlimg2.Contains("image"))
                    {
                        key2 = urlimg2.Remove(0, 7);
                        Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                    }
                    else
                    {
                        key2 = "null";
                        baseurl = "";
                        Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                    }
                    image_url = baseurl + key2;
                    Console.WriteLine(baseurl + key2);
                }
                if (userInfo.ContainsKey(new NSString("content_url")))
                    content_url = (userInfo[new NSString("content_url")] as NSString).ToString();
                if (userInfo.ContainsKey(new NSString("_key")))
                    _key = (userInfo[new NSString("_key")] as NSString).ToString();

                NSUserDefaults.StandardUserDefaults.SetString(image_url, "ImageNotification");
                NSUserDefaults.StandardUserDefaults.SetString(description, "DescriptionNotification");
                NSUserDefaults.StandardUserDefaults.SetString(_key, "KeyNotification");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "isPushNotification");

                Console.WriteLine("object:" + title + "|" + description + "|" + image_url + "|" + content_url + "|" + _key);

                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }
                UIViewController lp = storyboard.InstantiateViewController("Notification");
                //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                TopController().PresentModalViewController(lp, true);
                //processNotification(userInfo, false);
            }
        }

        void processNotification(NSDictionary options, bool fromFinishedLaunching)
        {
            //Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
            if (null != options && options.ContainsKey(new NSString("aps")))
            {
                //Get the aps dictionary
                NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

                string alert = string.Empty;
                string sound = string.Empty;
                int badge = -1;

                string title = string.Empty;
                string description = string.Empty;
                string image_url = string.Empty;
                string content_url = string.Empty;
                string _key = string.Empty;


                if (options.ContainsKey(new NSString("title")))           
                    title = (options[new NSString("title")] as NSString).ToString();
                if (options.ContainsKey(new NSString("description")))
                    description = (options[new NSString("description")] as NSString).ToString();
                if (options.ContainsKey(new NSString("image_url")))
                {
                    string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/roccogiocattoli/";
                    string urlimg2 = (options[new NSString("image_url")] as NSString).ToString();
                    string key2 = urlimg2.Remove(0, 7);
                    image_url = baseurl + key2;
                    Console.WriteLine(baseurl + key2);
                }
                if (options.ContainsKey(new NSString("content_url")))
                    content_url = (options[new NSString("content_url")] as NSString).ToString();
                if (options.ContainsKey(new NSString("_key")))
                    _key = (options[new NSString("_key")] as NSString).ToString();



                Console.WriteLine("object:"+title+"|"+description+"|"+image_url+"|"+content_url+"|"+_key);
                //Extract the alert text
                //NOTE: If you're using the simple alert by just specifying "  aps:{alert:"alert msg here"}  "
                //      this will work fine.  But if you're using a complex alert with Localization keys, etc., your "alert" object from the aps dictionary
                //      will be another NSDictionary... Basically the json gets dumped right into a NSDictionary, so keep that in mind
                if (aps.ContainsKey(new NSString("alert")))
                    alert = (aps[new NSString("alert")] as NSString).ToString();

                //Extract the sound string
                if (aps.ContainsKey(new NSString("sound")))
                    sound = (aps[new NSString("sound")] as NSString).ToString();
                //Extract the badge
                if (aps.ContainsKey(new NSString("badge")))
                {
                    string badgeStr = (aps[new NSString("badge")] as NSObject).ToString();
                    int.TryParse(badgeStr, out badge);
                }


                for(int a =0; a < aps.Keys.Length; a++)
                {
                    Console.WriteLine(aps.Keys[a].ToString());
                }

                for (int b = 0; b < options.Keys.Length; b++)
                {
                    Console.WriteLine(options.Keys[b].ToString());
                }

                if (aps.ContainsKey(new NSString("payload")))
                {
                    Console.WriteLine((aps[new NSString("payload")] as NSObject).ToString());
                }


                //If this came from the ReceivedRemoteNotification while the app was running,
                // we of course need to manually process things like the sound, badge, and alert.
                if (!fromFinishedLaunching)
                {
                    //Manually set the badge in case this came from a remote notification sent while the app was open
                    if (badge >= 0)
                        UIApplication.SharedApplication.ApplicationIconBadgeNumber = badge;
                    //Manually play the sound
                    if (!string.IsNullOrEmpty(sound))
                    {
                        //This assumes that in your json payload you sent the sound filename (like sound.caf)
                        // and that you've included it in your project directory as a Content Build type.
                        var soundObj = SystemSound.FromFile(sound);
                        //soundObj.PlaySystemSound();
                    }

                    //Manually show an alert
                    if (!string.IsNullOrEmpty(alert))
                    {
                        NotificationAlert(alert);
                    }
                }
            }

        }

        public async void NotificationAlert(string alert)
        {
            UIStoryboard storyboard;
            int button = await ShowAlert("RoccoGiocattoli", alert, "Ok", "Annulla");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }
                NSUserDefaults.StandardUserDefaults.SetBool(true, "isNotification");
                UIViewController lp = storyboard.InstantiateViewController("Notification");
                TopController().PresentModalViewController(lp, true);
            }
            else { }
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            Console.WriteLine("local" + application.ApplicationState.ToString());
            if (application.ApplicationState == UIApplicationState.Inactive || application.ApplicationState == UIApplicationState.Background)
            {
                if (Background == false)
                {
                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
                    NSUserDefaults.StandardUserDefaults.SetBool(true, "isBeaconNotification");

                    UIStoryboard storyboard = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                    }
                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    TopController().PresentModalViewController(lp, true);
                }
                else
                {
                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
                    NSUserDefaults.StandardUserDefaults.SetBool(true, "isBeaconNotification");

                    UIStoryboard storyboard = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                    }
                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    TopController().PresentModalViewController(lp, true);
                }

            }
            if (application.ApplicationState == UIApplicationState.Active)
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                UIStoryboard storyboard = new UIStoryboard();
                NSUserDefaults.StandardUserDefaults.SetBool(true, "isBeaconNotification");
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }

                UIViewController lp = storyboard.InstantiateViewController("Notification");
                TopController().PresentModalViewController(lp, true);
            }
        }

        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            // We need to handle URLs by passing them to their own OpenUrl in order to make the SSO authentication works.
            return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
        }

        public RootViewController RootViewController { get { return Window.RootViewController as RootViewController; } }

        // This method is invoked when the application is about to move from active to inactive state.
        // OpenGL applications should use this method to pause.
        public override void OnResignActivation(UIApplication application)
        {
        }
        // This method is called as part of the transiton from background to active state.
        public override void WillEnterForeground(UIApplication application)
        {
            
            Console.WriteLine("Riparti:"+ NSUserDefaults.StandardUserDefaults.BoolForKey("TokenRoccoN4U"));
        }

        // This method is called when the application is about to terminate. Save data, if needed.
        public override void WillTerminate(UIApplication application)
        {
        }

        // This method should be used to release shared resources and it should store the application state.
        // If your application supports background exection this method is called instead of WillTerminate
        // when the user quits.
        public override void DidEnterBackground(UIApplication application)
        {
            Console.WriteLine("Back:"+ NSUserDefaults.StandardUserDefaults.BoolForKey("TokenRoccoN4U"));
            Background = true;
            
            //locationmanager.RequestAlwaysAuthorization (); 
            //locationmanager.StartMonitoring (beaconRegion);
            //locationmanager.start
            //while (true) {
            //System.Threading.Thread.Sleep (100);
            //locationmanager.StartRangingBeacons (beaconRegion);
            //}
        }

       

        public void startBeacon()
        {

            beaconUUID = new NSUuid(uuid);
            beaconRegion = new CLBeaconRegion(beaconUUID, beaconId);


            beaconRegion.NotifyEntryStateOnDisplay = true;
            beaconRegion.NotifyOnEntry = true;
            beaconRegion.NotifyOnExit = true;

            locationmanager = new CLLocationManager();


            Console.WriteLine("Beacons");

            locationmanager.RegionEntered += (object sender, CLRegionEventArgs e) =>
            {
                Console.WriteLine("Region Entered");
                //var notification = new UILocalNotification () { AlertBody = "Uno stand è vicino!"  };
                //UIApplication.SharedApplication.CancelAllLocalNotifications();
                //UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
            };


            locationmanager.DidRangeBeacons += (object sender, CLRegionBeaconsRangedEventArgs e) =>
            {
                string TokenN4U = AppDelegate.Instance.TokenN4U;
                Console.WriteLine("Token");

                if (!blocco)
                {
                    if (AppDelegate.Instance.flagTokenN4U == true /*&& ListMessaggi.Count == 0*/ )
                    {
                        Console.WriteLine("Token1");
                        if (TokenN4U != null && TokenN4U != "")
                        {
                            Console.WriteLine("Token2:" + TokenN4U);
                            var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                            var requestMessage = new RestRequest("active-messages", Method.GET);
                            requestMessage.AddHeader("content-type", "application/json");
                            requestMessage.AddHeader("Net4U-Company", "roccogiocattoli");
                            requestMessage.AddHeader("Net4U-Token", TokenN4U);

                            client.Timeout = 60000;
                            //IRestResponse response = client.Execute(requestMessage);
                            blocco = true;
                            client.ExecuteAsync(requestMessage, response =>
                            {
                                Console.WriteLine("Token3:" + response.StatusCode);
                                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                                {
                                    blocco = false;
                                }
                                else
                                {
                                    //\	string prova = "{\"messages\":[{\"title\":\"titolo\",\"description\":\"descrizione\",\"image_url\":\"/image/4fae906a-0e25-4f1d-953c-6d8cf1cc0219\",\"content_url\":\"url\",\"type\":\"ibeacon\",\"timestamp_start\":1429800825,\"timestamp_end\":1450882425,\"distance\":5,\"_type\":\"message\",\"_key\":\"message::e68461b7-13f9-4296-856e-3bf83ffd587a\"}]}";
                                    Console.WriteLine(response.Content);
                                    JsonValue json = JsonValue.Parse(response.Content);
                                    JsonValue data = json["messages"];

                                    foreach (JsonValue dataItem in data)
                                    {
                                        var titolo = dataItem["title"];
                                        var urlimg = dataItem["image_url"];
                                        var descr = dataItem["description"];
                                        var action = dataItem["content_url"];
                                        var distanzaAp = dataItem["threshold"];
                                        float distanzaFl = 0;
                                        if (distanzaAp.ToString().Contains("."))
                                        {
                                            string distStr = distanzaAp.ToString().Replace('.', ',');
                                            distanzaFl = float.Parse(distStr);
                                        }
                                        else
                                        {
                                            distanzaFl = float.Parse(distanzaAp.ToString());
                                        }
                                        Console.WriteLine(distanzaFl);
                                        int distanza = (int)(distanzaFl * 100);
                                        Console.WriteLine(distanza);
                                        var key = dataItem["_key"];

                                        string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/roccogiocattoli/";
                                        string urlimg2 = urlimg;
                                        string key2 = "";
                                        if (urlimg2.Contains("image"))
                                        {
                                            key2 = urlimg2.Remove(0, 7);
                                            Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                                        }
                                        else
                                        {
                                            key2 = "null";
                                            baseurl = "";
                                            Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                                        }

                                        Console.WriteLine(titolo + "  " + baseurl + key2 + "   " + descr + "   " + action);

                                        ListMessaggi.Add(new Messaggio(titolo, baseurl + key2, descr, action, key, false));

                                        var client2 = new RestClient("http://api.netwintec.com:81/");
                                        var requestBeacon = new RestRequest("active-messages/" + key, Method.GET);
                                        requestBeacon.AddHeader("content-type", "application/json");
                                        requestBeacon.AddHeader("Net4U-Company", "roccogiocattoli");
                                        requestBeacon.AddHeader("Net4U-Token", TokenN4U);

                                        IRestResponse response2 = client2.Execute(requestBeacon);

                                        Console.WriteLine(response2.Content);

                                        //string prova2 = "{\"beacons\":[{\"uuid\": \"ACFD065E-C3C0-11E3-9BBE-1A514932AC01\",\"major\": 101,\"minor\": 5},{\"uuid\": \"ACFD065E-C3C0-11E3-9BBE-1A514932AC01\",\"major\": 101,\"minor\": 4}]}";

                                        JsonValue json2 = JsonValue.Parse(response2.Content);
                                        JsonValue data2 = json2["beacons"];

                                        foreach (JsonValue dataItem2 in data2)
                                        {
                                            var uuid2 = dataItem2["uuid"];
                                            var majorAp = dataItem2["major"];
                                            int major = int.Parse(majorAp.ToString());
                                            var minorAp = dataItem2["minor"];
                                            int minor = int.Parse(minorAp.ToString());
                                            Console.WriteLine(uuid2 + "|" + major + "|" + minor + "|" + distanza);
                                            string Appoggio;
                                            Appoggio = major.ToString() + "," + minor.ToString() + "," + i;
                                            ListBeacon.Add(Appoggio, new Beacon(uuid2, major, minor, distanza, i));
                                            flagBeacon.Add(Appoggio, false);
                                            if (flagBeacon.ContainsKey(Appoggio) == true)
                                            {
                                                Console.WriteLine("c'è");
                                            }
                                        }
                                        i++;
                                    }
                                    blocco = false;
                                    AppDelegate.Instance.flagTokenN4U = false;
                                    Console.WriteLine("Fine");
                                }
                            });

                        }
                    }
                    else
                    {
                        if (AppDelegate.Instance.Scan == true)
                        {
                            if (e.Beacons.Length > 0)
                            {
                                for (int a = 0; a < e.Beacons.Length; a++)
                                {
                                    var beacon = e.Beacons[a];

                                    Console.WriteLine(a + "   " + beacon.Major.ToString() + "  " + beacon.Minor.ToString() + "  " + beacon.Rssi);
                                    string App2;
                                    List<string> BeacAppoggioCount = new List<string>();

                                    int countBeac = 0;
                                    for (int q = 0; q < ListMessaggi.Count; q++)
                                    {
                                        App2 = beacon.Major.ToString() + "," + beacon.Minor.ToString() + "," + q;
                                        if (flagBeacon.ContainsKey(App2) == true)
                                        {

                                            BeacAppoggioCount.Add(App2);
                                            countBeac++;

                                        }
                                    }
                                    Console.WriteLine(countBeac);
                                    if (countBeac != 0)
                                    {
                                        for (int w = 0; w < countBeac; w++)
                                        {
                                            Beacon BeaconApp;
                                            BeaconApp = ListBeacon[BeacAppoggioCount[w]];

                                            if (ListMessaggi[BeaconApp.IdMessaggio].Visto == false)
                                            {
                                                nint distRssi = 0;

                                                if (BeaconApp.Distance <= 100)
                                                    distRssi = (nint)(-60);

                                                if (BeaconApp.Distance > 100 && BeaconApp.Distance <= 500)
                                                    distRssi = (nint)(-67);

                                                if (BeaconApp.Distance > 500 && BeaconApp.Distance <= 1500)
                                                    distRssi = (nint)(-83);

                                                if (BeaconApp.Distance > 1500)
                                                    distRssi = (nint)(-90);

                                                if (beacon.Rssi > distRssi)
                                                {
                                                    Messaggio MessaggioApp;
                                                    MessaggioApp = ListMessaggi[BeaconApp.IdMessaggio];
                                                    flagBeacon[BeacAppoggioCount[w]] = true;
                                                    ListMessaggi[BeaconApp.IdMessaggio].Visto = true;


                                                    var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                                                    //client.Authenticator = new HttpBasicAuthenticator(username, password);

                                                    //var requestN4U = new RestRequest("login/facebook?access_token=" + TokenFB, Method.GET);
                                                    var requestN4U = new RestRequest("notification/beaconreceive/" + MessaggioApp._key, Method.GET);
                                                    requestN4U.AddHeader("content-type", "application/json");
                                                    requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
                                                    requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));

                                                    try { 
                                                        client.Execute(requestN4U);
                                                    }
                                                    catch (Exception ee)
                                                    {
                                                        Console.WriteLine("beaconreceive");
                                                    }

                                                    var notification = new UILocalNotification() { AlertBody = MessaggioApp.Titolo, AlertTitle = "Rocco Giocattoli" };
                                                    notification.SoundName = UILocalNotification.DefaultSoundName;
                                                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 1;
                                                    UIApplication.SharedApplication.CancelAllLocalNotifications();
                                                    UIApplication.SharedApplication.PresentLocalNotificationNow(notification);

                                                    /*UIStoryboard storyboard = new UIStoryboard();
                                                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                                                    {
                                                        storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                                                    }
                                                    else
                                                    {
                                                        storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                                                    }

                                                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                                                    Console.WriteLine("Root:"+Window.RootViewController);
                                                    TopController().PresentModalViewController(lp, true);*/

                                                    NSUserDefaults.StandardUserDefaults.SetString(MessaggioApp.UrlImage, "ImageNotification");
                                                    NSUserDefaults.StandardUserDefaults.SetString(MessaggioApp.Descrizione, "DescriptionNotification");
                                                    NSUserDefaults.StandardUserDefaults.SetString(MessaggioApp._key, "KeyNotification");
                                                    Console.WriteLine(MessaggioApp.Titolo + "   " + MessaggioApp.Descrizione);
                                                    w = ListMessaggi.Count + 1;
                                                    a = e.Beacons.Length + 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            Console.WriteLine(UIDevice.CurrentDevice.SystemVersion+"|"+ UIDevice.CurrentDevice.CheckSystemVersion(8, 0));
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                locationmanager.RequestAlwaysAuthorization();
            locationmanager.StartMonitoring(beaconRegion);
            locationmanager.StartRangingBeacons(beaconRegion);


        }

        public UIViewController TopController()
        {
            UIViewController topController = (UIApplication.SharedApplication).KeyWindow.RootViewController;
            while (topController.PresentedViewController != null)
            {
                topController = topController.PresentedViewController;
            }

            return topController;
        }

    }

    public class Beacon
    {

        public string UUID;
        public int Major;
        public int Minor;
        public int Distance;
        public int IdMessaggio;

        public Beacon(string uuid, int major, int minor, int distance, int idmex)
        {
            UUID = uuid;
            Major = major;
            Minor = minor;
            Distance = distance;
            IdMessaggio = idmex;
        }

    }

    public class Messaggio
    {

        public string Titolo;
        public string UrlImage;
        public string Descrizione;
        public string Action;
        public string _key;
        public bool Visto;

        public Messaggio(string titolo, string urlimg, string descr, string action,string key, bool visto)
        {
            Titolo = titolo;
            UrlImage = urlimg;
            Descrizione = descr;
            Action = action;
            _key = key;
            Visto = visto;
        }
    }
}
