using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using SWRevealViewControllerBinding;
using MessageUI;



namespace RoccoGiocattoli
{
	partial class MenuController : UITableViewController
    {
        MFMailComposeViewController mailController;
        string[] to = new string[] { "info@roccogiocattoli.com" };
        public MenuController(IntPtr handle)
          : base(handle)
        { }

        public static MenuController Self { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var tableView = View as UITableView;
            if (tableView != null)
            {
                tableView.Source = new TableSource();
            }
           

            if (MFMailComposeViewController.CanSendMail)
            {
                mailController = new MFMailComposeViewController();
                mailController.SetToRecipients(to);
                mailController.Finished += (object s, MFComposeResultEventArgs args) =>
                {

                    Console.WriteLine(args.Result.ToString());

                    BeginInvokeOnMainThread(() =>
                    {
                        args.Controller.DismissViewController(true, null);
                    });
                };
            }

            MenuController.Self = this;
        }

        public void SendEmail()
        {
            if (MFMailComposeViewController.CanSendMail)
            {
                this.PresentViewController(mailController, true, null);
            }
            else
            {
                new UIAlertView("Mail not supported", "Can't send mail from this device", null, "OK");
            }
        }


        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
            
        }

    }
}

