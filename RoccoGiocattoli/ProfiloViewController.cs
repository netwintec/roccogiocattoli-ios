﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using SWRevealViewControllerBinding;
using CoreGraphics;
using RestSharp;
using System.Collections.Generic;
using CoreAnimation;

namespace RoccoGiocattoli
{
	partial class ProfiloViewController : UIViewController
	{
        UIButton utenteButton, premiButton, acquistiButton;
        UIView utenteView, premiView, acquistiView;
        int porta = 81;
        JsonUtility ju = new JsonUtility();
        List<int> ListPunti = new List<int>();
        List<int> ListBuono = new List<int>();
        List<string> ListDatiTessera = new List<string>();
        List<AcquistiObject> ListAquistiObject = new List<AcquistiObject>();
        int saldoPunti;
        string EanTessera;
        bool flagA1 = false, flagA2 = false, flagA3 = false, flagA4 = false, flagA5 = false, flagA6 = false, flagA7 = false, flagA8 = false, flagA9 = false, flagA10 = false;
        double  contentH;
        UILabel Titolo;
        UIImageView LoadingImage, stelline;
        bool isPhone,errorData;

        public static ProfiloViewController Self { get; private set; }

        public ProfiloViewController (IntPtr handle) : base (handle)
		{
		}


        public override void ViewDidAppear(bool animated)
        {
            Console.WriteLine("Token:"+ NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));
            base.ViewDidAppear(animated);

            //********Ritiro dati per profilo********
            var client = new RestClient("http://api.netwintec.com:" + porta + "/");
            //client.Authenticator = new HttpBasicAuthenticator(username, password);

            var requestN4U = new RestRequest("saldo", Method.GET);
            requestN4U.AddHeader("content-type", "application/json");
            requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
            requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));  // "e74e8d8c-b4c5-482b-9f0e-d5fe2f657c1b"
            
            IRestResponse response = client.Execute(requestN4U);

            Console.WriteLine(response.Content + "  " + response.StatusCode);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                
                ju.SpacchettamentoJsonSaldo(ListDatiTessera, response.Content);
            }
            else
            {
                ListDatiTessera.Add("0");
                ListDatiTessera.Add("");
            }

            Console.WriteLine(ListDatiTessera[0] + "  " + ListDatiTessera[1]);
            EanTessera = ListDatiTessera[1];
            saldoPunti = int.Parse(ListDatiTessera[0]);

            //*************************************

            //********Ritiro dati per acquisto********
            var client2 = new RestClient("http://api.netwintec.com:" + porta + "/");
            //client.Authenticator = new HttpBasicAuthenticator(username, password);

            var requestN4U2 = new RestRequest("movimenti", Method.GET);
            requestN4U2.AddHeader("content-type", "application/json");
            requestN4U2.AddHeader("Net4U-Company", "roccogiocattoli");
            requestN4U2.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));  // "e74e8d8c-b4c5-482b-9f0e-d5fe2f657c1b"
            

            IRestResponse response2 = client2.Execute(requestN4U2);

            Console.WriteLine(response2.Content + "  " +response2.StatusCode);
            if (response2.StatusCode == System.Net.HttpStatusCode.OK)
            {
                
                ju.SpacchettamentoJsonMovimenti(ListAquistiObject, response2.Content);
            }


            //*************************************

            //********Ritiro dati per premi********
            var client3 = new RestClient("http://api.netwintec.com:" + porta + "/");
            //client.Authenticator = new HttpBasicAuthenticator(username, password);

            var requestN4U3 = new RestRequest("premi", Method.GET);
            requestN4U3.AddHeader("content-type", "application/json");
            requestN4U3.AddHeader("Net4U-Company", "roccogiocattoli");
            requestN4U3.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));  // "e74e8d8c-b4c5-482b-9f0e-d5fe2f657c1b"
            

            IRestResponse response3 = client3.Execute(requestN4U3);
            Console.WriteLine(response3.Content + "  " + response3.StatusCode);
            if (response3.StatusCode == System.Net.HttpStatusCode.OK)
            {
               
                ju.SpacchettamentoJsonPremi(ListPunti, ListBuono, response3.Content);
                errorData = false;
            }
            else
            {
                errorData = true;

                ListPunti.Add(0);
                ListBuono.Add(0);

                ListPunti.Add(0);
                ListBuono.Add(0);

                ListPunti.Add(0);
                ListBuono.Add(0);

                ListPunti.Add(0);
                ListBuono.Add(0);

                ListPunti.Add(0);
                ListBuono.Add(0);

                ListPunti.Add(0);
                ListBuono.Add(0);
            }
            //*************************************

            LoadingImage.Layer.RemoveAllAnimations();
            LoadingImage.Alpha = 0;
            Titolo.Alpha = 1;
            ContentView.Alpha = 1;
            HeaderView.Alpha = 1;
            stelline.Alpha = 1;


            //Creazioni View 1
            utenteView = new UIView(new CGRect(0, 0, View.Frame.Width, contentH));

            int yyUtente = 0;
            UIScrollView scrollViewUtente = new UIScrollView(new CGRect(0, 0, View.Frame.Width, contentH));

            UILabel Nome, Cognome;

            if (isPhone)
            {
                Nome = new UILabel(new CGRect(10, 10, View.Frame.Width / 2 - 12, 25));
                Nome.Text = NSUserDefaults.StandardUserDefaults.StringForKey("NomeRoccoN4U");
                Nome.Font = UIFont.BoldSystemFontOfSize(14);
                Nome.TextAlignment = UITextAlignment.Right;
                //Nome.BackgroundColor = UIColor.Blue;


                Cognome = new UILabel(new CGRect(View.Frame.Width / 2 + 2, 10, View.Frame.Width / 2 - 12, 25));
                Cognome.Text = NSUserDefaults.StandardUserDefaults.StringForKey("CognomeRoccoN4U");
                Cognome.Font = UIFont.BoldSystemFontOfSize(14);
                Cognome.TextAlignment = UITextAlignment.Left;
                //Cognome.BackgroundColor = UIColor.Blue;

                yyUtente += 35;
            }
            else
            {
                Nome = new UILabel(new CGRect(10, 10, View.Frame.Width / 2 - 15, 30));
                Nome.Text = NSUserDefaults.StandardUserDefaults.StringForKey("NomeRoccoN4U");
                Nome.Font = UIFont.BoldSystemFontOfSize(25);
                Nome.TextAlignment = UITextAlignment.Right;


                Cognome = new UILabel(new CGRect(View.Frame.Width / 2 + 5, 10, View.Frame.Width / 2 - 15, 30));
                Cognome.Text = NSUserDefaults.StandardUserDefaults.StringForKey("CognomeRoccoN4U");
                Cognome.Font = UIFont.BoldSystemFontOfSize(25);
                Cognome.TextAlignment = UITextAlignment.Left;

                yyUtente += 40;
            }


            UIImageView imageBarCode;

            if (EanTessera != "")
            {
                if (isPhone)
                {
                    imageBarCode = new UIImageView(new CGRect(View.Frame.Width / 2 - 100, yyUtente + 15, 200, 70));


                    var barcodewriter = new ZXing.Mobile.BarcodeWriter
                    {
                        Format = ZXing.BarcodeFormat.CODE_128,
                        Options = new ZXing.Common.EncodingOptions
                        {
                            Width = (int)(200),
                            Height = (int)(70)
                        }
                    };
                    var barcode = barcodewriter.Write(EanTessera);
                    imageBarCode.Image = barcode;

                    yyUtente += 85;

                }
                else
                {
                    imageBarCode = new UIImageView(new CGRect(View.Frame.Width / 2 - 150, yyUtente + 15, 300, 105));


                    var barcodewriter = new ZXing.Mobile.BarcodeWriter
                    {
                        Format = ZXing.BarcodeFormat.CODE_128,
                        Options = new ZXing.Common.EncodingOptions
                        {
                            Width = (int)(300),
                            Height = (int)(105)
                        }
                    };
                    var barcode = barcodewriter.Write(EanTessera);
                    imageBarCode.Image = barcode;

                    yyUtente += 120;

                }

            }
            else
            {
                imageBarCode = new UIImageView(new CGRect(0, 0, 0, 0));
            }

            UILabel Codice;

            if (isPhone)
            {
                Codice = new UILabel(new CGRect(View.Frame.Width / 2 - 150, yyUtente + 10, 300, 25));
                Codice.Text = "N° carta " + EanTessera;
                Codice.Font = UIFont.BoldSystemFontOfSize(13);
                Codice.TextAlignment = UITextAlignment.Center;
                yyUtente += 35;

            }
            else
            {
                Codice = new UILabel(new CGRect(View.Frame.Width / 2 - 200, yyUtente + 10, 400, 30));
                Codice.Text = "N° carta " + EanTessera;
                Codice.Font = UIFont.BoldSystemFontOfSize(22);
                Codice.TextAlignment = UITextAlignment.Center;
                yyUtente += 40;

            }

            UIView Bordo;
            if (isPhone)
            {
                Bordo = new UIView(new CGRect(30, yyUtente + 20, View.Frame.Width - 60, ((View.Frame.Width - 60) * 110) / 320));
            }
            else
            {
                Bordo = new UIView(new CGRect(80, yyUtente + 20, View.Frame.Width - 160, ((View.Frame.Width - 160) * 110) / 320));
            }
            Bordo.BackgroundColor = UIColor.FromRGB(81, 158, 200);
            Bordo.Layer.CornerRadius = 10;

            UIView Interno = new UIView(new CGRect(3, 3, Bordo.Frame.Width - 6, Bordo.Frame.Height - 6));
            Interno.BackgroundColor = UIColor.White;
            Interno.Layer.CornerRadius = 7;
            Bordo.AddSubview(Interno);

            UILabel Label;
            if (isPhone)
            {
                Label = new UILabel(new CGRect(Interno.Frame.Width / 2 - 100, 0, 200, Interno.Frame.Height / 2));
                Label.Text = "Totale Petali Acquisiti";
                Label.Lines = 1;
                Label.Font = UIFont.FromName("BubblegumSans-Regular", 17);
                Label.TextAlignment = UITextAlignment.Center;
                Label.TextColor = UIColor.FromRGB(233, 69, 141);
            }
            else
            {
                Label = new UILabel(new CGRect(Interno.Frame.Width / 2 - 150, 0, 300, Interno.Frame.Height / 2));
                Label.Text = "Totale Petali Acquisiti";
                Label.Lines = 1;
                Label.Font = UIFont.FromName("BubblegumSans-Regular", 30);
                Label.TextAlignment = UITextAlignment.Center;
                Label.TextColor = UIColor.FromRGB(233, 69, 141);
            }

            UILabel Punti;
            if (isPhone)
            {
                Punti = new UILabel(new CGRect(Interno.Frame.Width / 2 - 100, Interno.Frame.Height / 2, 200, Interno.Frame.Height / 2));
                Punti.Text = saldoPunti.ToString();
                Punti.Lines = 1;
                Punti.Font = UIFont.FromName("BubblegumSans-Regular", 23);
                Punti.TextAlignment = UITextAlignment.Center;
                Punti.TextColor = UIColor.FromRGB(233, 69, 141);
            }
            else
            {
                Punti = new UILabel(new CGRect(Interno.Frame.Width / 2 - 150, Interno.Frame.Height / 2, 300, Interno.Frame.Height / 2));
                Punti.Text = saldoPunti.ToString();
                Punti.Lines = 1;
                Punti.Font = UIFont.FromName("BubblegumSans-Regular", 33);
                Punti.TextAlignment = UITextAlignment.Center;
                Punti.TextColor = UIColor.FromRGB(233, 69, 141);
            }

            Interno.AddSubview(Label);
            Interno.AddSubview(Punti);

            UIImageView coccinella;

            if (isPhone)
            {
                nfloat imageW = ((((View.Frame.Width - 60) * 110) / 320) * 8) / 11;
                nfloat imageH = ((114 * imageW) / 94);
                coccinella = new UIImageView(new CGRect(33, yyUtente + 20 + (((View.Frame.Width - 60) * 110) / 320) - ((imageW * 52) / 48.75), imageW, imageH));
                coccinella.Image = UIImage.FromFile("coccinella.png");
                yyUtente += (int)(((View.Frame.Width - 60) * 110) / 320) + 35;
            }
            else
            {
                nfloat imageW = ((((View.Frame.Width - 160) * 110) / 320) * 8) / 11;
                nfloat imageH = ((114 * imageW) / 94);
                coccinella = new UIImageView(new CGRect(83, yyUtente + 20 + (((View.Frame.Width - 160) * 110) / 320) - ((imageW * 52) / 48.75), imageW, imageH));
                coccinella.Image = UIImage.FromFile("coccinella.png");
                yyUtente += (int)(((View.Frame.Width - 160) * 110) / 320) + 35;
            }

            Console.WriteLine(coccinella.Bounds + "    " + coccinella.Frame + "  ");



            UIImageView banner;
            UILabel Complimenti, Text, Buono;
            int soldiBuono = 0;

            if (saldoPunti >= ListPunti[0])
            {
                soldiBuono++;
            }
            if (saldoPunti >= ListPunti[1])
            {
                soldiBuono++;
            }
            if (saldoPunti >= ListPunti[2])
            {
                soldiBuono++;
            }
            if (saldoPunti >= ListPunti[3])
            {
                soldiBuono++;
            }
            if (saldoPunti >= ListPunti[4])
            {
                soldiBuono++;
            }
            if (soldiBuono > 0 && !errorData)
            {
                if (isPhone)
                {
                    banner = new UIImageView(new CGRect((View.Frame.Width / 2) - 125, yyUtente + 20, 250, 196));
                    banner.Image = UIImage.FromFile("banner.png");

                    Complimenti = new UILabel(new CGRect((View.Frame.Width / 2) - 125, yyUtente + 20 + 20, 250, 25));
                    Complimenti.Text = "COMPLIMENTI!";
                    Complimenti.Font = UIFont.BoldSystemFontOfSize(17);
                    Complimenti.TextAlignment = UITextAlignment.Center;
                    Complimenti.TextColor = UIColor.White;
                    Text = new UILabel(new CGRect((View.Frame.Width / 2) - 125, yyUtente + 20 + 55, 250, 45));
                    Text.Text = "Per te un\nBUONO SCONTO da";
                    Text.Font = UIFont.BoldSystemFontOfSize(17);
                    Text.TextAlignment = UITextAlignment.Center;
                    Text.TextColor = UIColor.White;
                    Text.Lines = 2;
                    Buono = new UILabel(new CGRect((View.Frame.Width / 2) - 125, yyUtente + 20 + 110, 250, 20));
                    Buono.Text = "€ " + ListBuono[soldiBuono - 1] + ",00";
                    Buono.Font = UIFont.BoldSystemFontOfSize(19);
                    Buono.TextAlignment = UITextAlignment.Center;
                    Buono.TextColor = UIColor.White;


                    yyUtente += 189;
                }
                else
                {
                    banner = new UIImageView(new CGRect((View.Frame.Width / 2) - 200, yyUtente + 20, 400, 313));
                    banner.Image = UIImage.FromFile("banner.png");

                    Complimenti = new UILabel(new CGRect((View.Frame.Width / 2) - 200, yyUtente + 20 + 40, 400, 30));
                    Complimenti.Text = "COMPLIMENTI!";
                    Complimenti.Font = UIFont.BoldSystemFontOfSize(25);
                    Complimenti.TextAlignment = UITextAlignment.Center;
                    Complimenti.TextColor = UIColor.White;
                    Text = new UILabel(new CGRect((View.Frame.Width / 2) - 200, yyUtente + 20 + 85, 400, 60));
                    Text.Text = "Per te un\nBUONO SCONTO da";
                    Text.Font = UIFont.BoldSystemFontOfSize(25);
                    Text.TextAlignment = UITextAlignment.Center;
                    Text.TextColor = UIColor.White;
                    Text.Lines = 2;
                    Buono = new UILabel(new CGRect((View.Frame.Width / 2) - 200, yyUtente + 20 + 175, 400, 30));
                    Buono.Text = "€ " + ListBuono[soldiBuono - 1] + ",00";
                    Buono.Font = UIFont.BoldSystemFontOfSize(27);
                    Buono.TextAlignment = UITextAlignment.Center;
                    Buono.TextColor = UIColor.White;

                    yyUtente += 245;
                }
            }
            else
            {
                banner = new UIImageView(new CGRect(0, 0, 0, 0));
                Complimenti = new UILabel(new CGRect(0, 0, 0, 0));
                Text = new UILabel(new CGRect(0, 0, 0, 0));
                Buono = new UILabel(new CGRect(0, 0, 0, 0));
            }

            scrollViewUtente.Add(Nome);
            scrollViewUtente.Add(Cognome);
            scrollViewUtente.Add(imageBarCode);
            scrollViewUtente.Add(Codice);
            scrollViewUtente.Add(Bordo);
            scrollViewUtente.Add(coccinella);
            scrollViewUtente.Add(banner);
            scrollViewUtente.Add(Complimenti);
            scrollViewUtente.Add(Text);
            scrollViewUtente.Add(Buono);

            scrollViewUtente.ContentSize = new CGSize(View.Frame.Width, yyUtente + 20);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            utenteView.Add(scrollViewUtente);

            //**************************************************************************

            //Creazioni View 2
            acquistiView = new UIView(new CGRect(0, 0, View.Frame.Width, contentH));

            UIView BordoArticoli;

            if (isPhone)
            {
                BordoArticoli = new UIView(new CGRect(10, 10, View.Frame.Width - 20, contentH - 20));
                BordoArticoli.BackgroundColor = UIColor.FromRGB(81, 158, 200);
                BordoArticoli.Layer.CornerRadius = 10;
            }
            else
            {
                BordoArticoli = new UIView(new CGRect(40, 10, View.Frame.Width - 80, contentH - 20));
                BordoArticoli.BackgroundColor = UIColor.FromRGB(81, 158, 200);
                BordoArticoli.Layer.CornerRadius = 10;
            }
            UIView Content = new UIView(new CGRect(3, 3, BordoArticoli.Frame.Width - 6, BordoArticoli.Frame.Height - 6));
            Content.BackgroundColor = UIColor.White;
            Content.Layer.CornerRadius = 7;
            BordoArticoli.AddSubview(Content);

            if (ListAquistiObject.Count == 0)
            {
                UILabel LabelAcquisti;
                if (isPhone)
                {
                    LabelAcquisti = new UILabel(new CGRect(10, Content.Frame.Height / 2 - 15, Content.Frame.Width - 20, 30));
                    LabelAcquisti.Text = "Nessun Acquisto recente";
                    LabelAcquisti.Lines = 1;
                    LabelAcquisti.Font = UIFont.FromName("BubblegumSans-Regular", 22);
                    LabelAcquisti.TextAlignment = UITextAlignment.Center;
                    //LabelAcquisti.TextColor = UIColor.FromRGB(233, 69, 141);
                }
                else
                {
                    LabelAcquisti = new UILabel(new CGRect(10, Content.Frame.Height / 2 - 20, Content.Frame.Width - 20, 40));
                    LabelAcquisti.Text = "Nessun Acquisto recente";
                    LabelAcquisti.Lines = 1;
                    LabelAcquisti.Font = UIFont.FromName("BubblegumSans-Regular", 32);
                    LabelAcquisti.TextAlignment = UITextAlignment.Center;
                    //LabelAcquisti.TextColor = UIColor.FromRGB(233, 69, 141);
                }
                Content.AddSubview(LabelAcquisti);
            }
            else
            {

                UITableView tabellaAcquisti = tabellaAcquisti = new UITableView(new CGRect(2, 2, Content.Frame.Width - 4, Content.Frame.Height - 4)); ;
                tabellaAcquisti.Source = new TableSourceAcquisti(ListAquistiObject, isPhone);
                tabellaAcquisti.SeparatorColor = UIColor.Clear;

                Content.AddSubview(tabellaAcquisti);

            }

            acquistiView.Add(BordoArticoli);

            //****************************************************************************

            //Creazioni View 3
            premiView = new UIView(new CGRect(0, 0, View.Frame.Width, contentH));

            int yyPremi = 0;
            nfloat padding, height, cella, font;
            if (isPhone)
            {
                padding = 20;
                height = 300;
                cella = 60;
                font = 15;
            }
            else
            {
                padding = 50;
                height = 400;
                cella = 80;
                font = 22;
            }


            UIScrollView scrollViewPremi = new UIScrollView(new CGRect(0, 0, View.Frame.Width, contentH));

            UILabel LabelPremi;
            if (isPhone)
            {
                LabelPremi = new UILabel(new CGRect(View.Frame.Width / 2 - 100, 10, 200, 30));
                LabelPremi.Text = "CATALOGO PREMI";
                LabelPremi.Font = UIFont.FromName("BubblegumSans-Regular", 18);
                LabelPremi.TextAlignment = UITextAlignment.Center;
                yyPremi += 40;
            }
            else {

                LabelPremi = new UILabel(new CGRect(View.Frame.Width / 2 - 100, 20, 200, 40));
                LabelPremi.Text = "CATALOGO PREMI";
                LabelPremi.Font = UIFont.FromName("BubblegumSans-Regular", 25);
                LabelPremi.TextAlignment = UITextAlignment.Center;
                yyPremi += 70;

            }

            UIView tabella = new UIView(new CGRect(padding, yyPremi + 10, View.Frame.Width - (2 * padding), height));

            UIView BordoImage1 = new UIView(new CGRect(0, 0, cella, cella));
            BordoImage1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
            UIImageView image1 = new UIImageView(new CGRect(3, 3, cella - 3, cella - 6));
            image1.Image = UIImage.FromFile("Margherita_spento_1.png");
            image1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
            BordoImage1.AddSubview(image1);
            tabella.AddSubview(BordoImage1);

            UIView BordoImage2 = new UIView(new CGRect(0, cella, cella, cella));
            BordoImage2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
            UIImageView image2 = new UIImageView(new CGRect(3, 3, cella - 3, cella - 6));
            image2.Image = UIImage.FromFile("Margherita_spento_2.png");
            image2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
            BordoImage2.AddSubview(image2);
            tabella.AddSubview(BordoImage2);

            UIView BordoImage3 = new UIView(new CGRect(0, 2 * cella, cella, cella));
            BordoImage3.BackgroundColor = UIColor.FromRGB(0, 160, 210);
            UIImageView image3 = new UIImageView(new CGRect(3, 3, cella - 3, cella - 6));
            image3.Image = UIImage.FromFile("Margherita_attivo_3.png");
            image3.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;
            BordoImage3.AddSubview(image3);
            tabella.AddSubview(BordoImage3);

            UIView BordoImage4 = new UIView(new CGRect(0, 3 * cella, cella, cella));
            BordoImage4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
            UIImageView image4 = new UIImageView(new CGRect(3, 3, cella - 3, cella - 6));
            image4.Image = UIImage.FromFile("Margherita_spento_4.png");
            image4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
            BordoImage4.AddSubview(image4);
            tabella.AddSubview(BordoImage4);

            UIView BordoImage5 = new UIView(new CGRect(0, 4 * cella, cella, cella));
            BordoImage5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
            UIImageView image5 = new UIImageView(new CGRect(3, 3, cella - 3, cella - 6));
            image5.Image = UIImage.FromFile("Margherita_spento_5.png");
            image5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
            BordoImage5.AddSubview(image5);
            tabella.AddSubview(BordoImage5);


            UIView BordoDatiTabella = new UIView(new CGRect(cella, 0, tabella.Frame.Width - cella, height));
            BordoDatiTabella.BackgroundColor = UIColor.FromRGB(0, 160, 210);

            UIView DavantiImage1 = new UIView(new CGRect(0, 3, 3, cella - 6));
            DavantiImage1.BackgroundColor = UIColor.FromRGB(0, 160, 210);
            BordoDatiTabella.AddSubview(DavantiImage1);

            UIView DavantiImage2 = new UIView(new CGRect(0, 3 + cella, 3, cella - 6));
            DavantiImage2.BackgroundColor = UIColor.FromRGB(0, 160, 210);
            BordoDatiTabella.AddSubview(DavantiImage2);

            UIView DavantiImage3 = new UIView(new CGRect(0, 3 + (2 * cella), 3, cella - 6));
            DavantiImage3.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;
            BordoDatiTabella.AddSubview(DavantiImage3);

            UIView DavantiImage4 = new UIView(new CGRect(0, 3 + (3 * cella), 3, cella - 6));
            DavantiImage4.BackgroundColor = UIColor.FromRGB(0, 160, 210);
            BordoDatiTabella.AddSubview(DavantiImage4);

            UIView DavantiImage5 = new UIView(new CGRect(0, 3 + (4 * cella), 3, cella - 6));
            DavantiImage5.BackgroundColor = UIColor.FromRGB(0, 160, 210);
            BordoDatiTabella.AddSubview(DavantiImage5);

            UIView DatiTabella = new UIView(new CGRect(3, 3, tabella.Frame.Width - cella - 6, height - 6));
            DatiTabella.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;
            BordoDatiTabella.AddSubview(DatiTabella);

            UILabel LabelPetali = new UILabel(new CGRect(DatiTabella.Frame.Width / 2 - 150, (height - 6) / 4 - 35, 300, 25));
            LabelPetali.Text = "PETALI/SCONTO";
            LabelPetali.Font = UIFont.BoldSystemFontOfSize(font);
            LabelPetali.TextColor = UIColor.Black;
            LabelPetali.TextAlignment = UITextAlignment.Center;

            UILabel PuntiPetali = new UILabel(new CGRect(DatiTabella.Frame.Width / 2 - 150, (height - 6) / 4 + 10, 300, 25));
            PuntiPetali.Text = ListPunti[2].ToString();
            PuntiPetali.Font = UIFont.BoldSystemFontOfSize(font);
            PuntiPetali.TextColor = UIColor.Black;
            PuntiPetali.TextAlignment = UITextAlignment.Center;

            UIView ViewSeparetor = new UIView(new CGRect(DatiTabella.Frame.Width / 2 - 20, (height - 6) / 2 - 1.5, 40, 3));
            ViewSeparetor.BackgroundColor = UIColor.FromRGB(0, 160, 210);

            UILabel LabelBuono = new UILabel(new CGRect(DatiTabella.Frame.Width / 2 - 150, (3 * (height - 6)) / 4 - 35, 300, 25));
            LabelBuono.Text = "BUONO DA";
            LabelBuono.Font = UIFont.BoldSystemFontOfSize(font);
            LabelBuono.TextColor = UIColor.Black;
            LabelBuono.TextAlignment = UITextAlignment.Center;

            UILabel PuntiBuoni = new UILabel(new CGRect(DatiTabella.Frame.Width / 2 - 150, (3 * (height - 6)) / 4 + 10, 300, 25));
            PuntiBuoni.Text = "€ " + ListBuono[2].ToString() + ",00";
            PuntiBuoni.Font = UIFont.BoldSystemFontOfSize(font);
            PuntiBuoni.TextColor = UIColor.Black;
            PuntiBuoni.TextAlignment = UITextAlignment.Center;

            DatiTabella.AddSubview(LabelPetali);
            DatiTabella.AddSubview(PuntiPetali);
            DatiTabella.AddSubview(ViewSeparetor);
            DatiTabella.AddSubview(LabelBuono);
            DatiTabella.AddSubview(PuntiBuoni);

            tabella.AddSubview(BordoDatiTabella);

            UITapGestureRecognizer image1Tap = new UITapGestureRecognizer(() =>
            {
                BordoImage1.BackgroundColor = UIColor.FromRGB(0, 160, 210);
                image1.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;
                DavantiImage1.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;

                BordoImage2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage2.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage3.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image3.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage3.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage4.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage5.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                PuntiPetali.Text = ListPunti[0].ToString();
                PuntiBuoni.Text = "€ " + ListBuono[0].ToString() + ",00";

                image1.Image = UIImage.FromFile("Margherita_attivo_1.png");
                image2.Image = UIImage.FromFile("Margherita_spento_2.png");
                image3.Image = UIImage.FromFile("Margherita_spento_3.png");
                image4.Image = UIImage.FromFile("Margherita_spento_4.png");
                image5.Image = UIImage.FromFile("Margherita_spento_5.png");

            });
            image1.UserInteractionEnabled = true;
            image1.AddGestureRecognizer(image1Tap);

            UITapGestureRecognizer image2Tap = new UITapGestureRecognizer(() =>
            {
                BordoImage2.BackgroundColor = UIColor.FromRGB(0, 160, 210);
                image2.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;
                DavantiImage2.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;

                BordoImage1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage1.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage3.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image3.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage3.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage4.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage5.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                PuntiPetali.Text = ListPunti[1].ToString();
                PuntiBuoni.Text = "€ " + ListBuono[1].ToString() + ",00";

                image2.Image = UIImage.FromFile("Margherita_attivo_2.png");
                image1.Image = UIImage.FromFile("Margherita_spento_1.png");
                image3.Image = UIImage.FromFile("Margherita_spento_3.png");
                image4.Image = UIImage.FromFile("Margherita_spento_4.png");
                image5.Image = UIImage.FromFile("Margherita_spento_5.png");

            });
            image2.UserInteractionEnabled = true;
            image2.AddGestureRecognizer(image2Tap);

            UITapGestureRecognizer image3Tap = new UITapGestureRecognizer(() =>
            {
                BordoImage3.BackgroundColor = UIColor.FromRGB(0, 160, 210);
                image3.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;
                DavantiImage3.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;

                BordoImage2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage2.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage1.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage4.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage5.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                PuntiPetali.Text = ListPunti[2].ToString();
                PuntiBuoni.Text = "€ " + ListBuono[2].ToString() + ",00";

                image3.Image = UIImage.FromFile("Margherita_attivo_3.png");
                image2.Image = UIImage.FromFile("Margherita_spento_2.png");
                image1.Image = UIImage.FromFile("Margherita_spento_1.png");
                image4.Image = UIImage.FromFile("Margherita_spento_4.png");
                image5.Image = UIImage.FromFile("Margherita_spento_5.png");


            });
            image3.UserInteractionEnabled = true;
            image3.AddGestureRecognizer(image3Tap);

            UITapGestureRecognizer image4Tap = new UITapGestureRecognizer(() =>
            {
                BordoImage4.BackgroundColor = UIColor.FromRGB(0, 160, 210);
                image4.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;
                DavantiImage4.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;

                BordoImage2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage2.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage3.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image3.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage3.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage1.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image5.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage5.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                PuntiPetali.Text = ListPunti[3].ToString();
                PuntiBuoni.Text = "€ " + ListBuono[3].ToString() + ",00";

                image4.Image = UIImage.FromFile("Margherita_attivo_4.png");
                image2.Image = UIImage.FromFile("Margherita_spento_2.png");
                image3.Image = UIImage.FromFile("Margherita_spento_3.png");
                image1.Image = UIImage.FromFile("Margherita_spento_1.png");
                image5.Image = UIImage.FromFile("Margherita_spento_5.png");

            });
            image4.UserInteractionEnabled = true;
            image4.AddGestureRecognizer(image4Tap);

            UITapGestureRecognizer image5Tap = new UITapGestureRecognizer(() =>
            {
                BordoImage5.BackgroundColor = UIColor.FromRGB(0, 160, 210);
                image5.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;
                DavantiImage5.BackgroundColor = UIColor.FromRGB(237, 237, 237); ;

                BordoImage2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image2.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage2.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage3.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image3.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage3.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image4.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage4.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                BordoImage1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                image1.BackgroundColor = UIColor.FromRGB(178, 178, 178);
                DavantiImage1.BackgroundColor = UIColor.FromRGB(0, 160, 210);

                PuntiPetali.Text = ListPunti[4].ToString();
                PuntiBuoni.Text = "€ " + ListBuono[4].ToString() + ",00";

                image5.Image = UIImage.FromFile("Margherita_attivo_5.png");
                image2.Image = UIImage.FromFile("Margherita_spento_2.png");
                image3.Image = UIImage.FromFile("Margherita_spento_3.png");
                image4.Image = UIImage.FromFile("Margherita_spento_4.png");
                image1.Image = UIImage.FromFile("Margherita_spento_1.png");


            });
            image5.UserInteractionEnabled = true;
            image5.AddGestureRecognizer(image5Tap);

            yyPremi += (int)(height + 10);

            scrollViewPremi.Add(LabelPremi);
            scrollViewPremi.Add(tabella);

            scrollViewPremi.ContentSize = new CGSize(View.Frame.Width, yyPremi + 10);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            premiView.Add(scrollViewPremi);

            ContentView.Add(utenteView);


        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ProfiloViewController.Self = this;

            if (this.RevealViewController() == null)
                return;

            

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                isPhone = true;
                contentH = View.Frame.Height - 204;
            }
            else
            {
                isPhone = false;

                contentH = View.Frame.Height - 224;
            }


            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));

            LoadingImage = new UIImageView(new CGRect(View.Frame.Width/2-40,View.Frame.Height/2-40,80,80));
            LoadingImage.Image = UIImage.FromFile("rocco_placeholder.png");

            stelline = new UIImageView(new CGRect(View.Frame.Width / 2 - 50, 120 , 100, 12));
            stelline.Image = UIImage.FromFile("stelline_blu.png");

            Titolo = new UILabel(new CGRect(0, 0, 180, 30));
            Titolo.Text = "IL MIO PROFILO";
            Titolo.Font = UIFont.FromName("BubblegumSans-Regular", 20);
            Titolo.TextAlignment = UITextAlignment.Center;
            TitoloView.AddSubview(Titolo);

            View.Add(stelline);
            View.Add(LoadingImage);
            Titolo.Alpha = 0;
            ContentView.Alpha = 0;
            HeaderView.Alpha = 0;
            stelline.Alpha = 0;

            CABasicAnimation rotationAnimation = new CABasicAnimation();
            rotationAnimation.KeyPath = "transform.rotation.z";
            rotationAnimation.To = new NSNumber(Math.PI * 2);
            rotationAnimation.Duration = 2;
            rotationAnimation.Cumulative = true;
            rotationAnimation.RepeatCount = float.MaxValue;
            LoadingImage.Layer.AddAnimation(rotationAnimation, "rotationAnimation");

            /*this.revealViewController().r = 0.0f;
            [self.revealButtonItem setTarget:self.revealViewController];
            [self.revealButtonItem setAction:@selector(rightRevealToggle: )];*/
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RightRevealToggleAnimated(true);
            //MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            if (isPhone) {
                utenteButton = new UIButton(new CGRect(20, 20, (View.Frame.Width - 80) / 3, 50));
                utenteButton.Font = UIFont.BoldSystemFontOfSize(15);
                utenteButton.ContentEdgeInsets = new UIEdgeInsets(4, 0, 0, 0);
            }
            else
            {
                utenteButton = new UIButton(new CGRect(20, 25, (View.Frame.Width - 80) / 3, 70));
                utenteButton.Font = UIFont.BoldSystemFontOfSize(22);
                utenteButton.ContentEdgeInsets = new UIEdgeInsets(4, 0, 0, 0);
            }
            Console.WriteLine(utenteButton.Frame);
            utenteButton.SetTitle("Utente", UIControlState.Normal);
            utenteButton.VerticalAlignment = UIControlContentVerticalAlignment.Top;
            utenteButton.SetTitleColor (UIColor.FromRGB(233, 69, 141),UIControlState.Normal);
            utenteButton.BackgroundColor = UIColor.White;
            utenteButton.Layer.CornerRadius = 10;
            
            utenteButton.TouchUpInside += (object sender, EventArgs e) => {
                ChangeView(0);
            };

            if (isPhone)
            {
                acquistiButton = new UIButton(new CGRect(((View.Frame.Width - 80) / 3) + 40, 20, (View.Frame.Width - 80) / 3, 50));
                acquistiButton.Font = UIFont.BoldSystemFontOfSize(15);
                acquistiButton.ContentEdgeInsets = new UIEdgeInsets(4, 0, 0, 0);
            }
            else
            {
                acquistiButton = new UIButton(new CGRect(((View.Frame.Width - 80) / 3) + 40, 25, (View.Frame.Width - 80) / 3, 70));
                acquistiButton.Font = UIFont.BoldSystemFontOfSize(22);
                acquistiButton.ContentEdgeInsets = new UIEdgeInsets(4, 0, 0, 0);
            }
            Console.WriteLine(acquistiButton.Frame);
            acquistiButton.SetTitle("Acquisti", UIControlState.Normal);
            acquistiButton.VerticalAlignment = UIControlContentVerticalAlignment.Top;
            acquistiButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            acquistiButton.BackgroundColor = UIColor.FromRGB(233, 69, 141);
            acquistiButton.Layer.CornerRadius = 10;

            acquistiButton.TouchUpInside += (object sender, EventArgs e) => {
                ChangeView(1);
            };

            if (isPhone)
            {
                premiButton = new UIButton(new CGRect((((View.Frame.Width - 80) / 3) * 2) + 60, 20, (View.Frame.Width - 80) / 3, 50));
                premiButton.Font = UIFont.BoldSystemFontOfSize(15);
                premiButton.ContentEdgeInsets = new UIEdgeInsets(4, 0, 0, 0);
            }
            else
            {
                premiButton = new UIButton(new CGRect((((View.Frame.Width - 80) / 3) * 2) + 60, 25, (View.Frame.Width - 80) / 3, 70));
                premiButton.Font = UIFont.BoldSystemFontOfSize(22);
                premiButton.ContentEdgeInsets = new UIEdgeInsets(4, 0, 0, 0);
            }
            Console.WriteLine(premiButton.Frame);
            premiButton.SetTitle("Premi", UIControlState.Normal);
            premiButton.VerticalAlignment = UIControlContentVerticalAlignment.Top;
            premiButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            premiButton.BackgroundColor = UIColor.FromRGB(233, 69, 141);
            premiButton.Layer.CornerRadius = 10;

            premiButton.TouchUpInside += (object sender, EventArgs e) => {
                ChangeView(2);
            };

            HeaderView.AddSubview(utenteButton);
            HeaderView.AddSubview(acquistiButton);
            HeaderView.AddSubview(premiButton);
            

            Console.WriteLine("Bounds" + ContentView.Bounds + "|Frame:" + ContentView.Frame+"|ContentH:"+contentH);
            

            Titolo.Alpha = 0;
            ContentView.Alpha = 0;
            HeaderView.Alpha = 0;
            stelline.Alpha = 0;
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public void ChangeView(int index)
        {
            //Change Button Color

            if (index == 0) {
                utenteButton.SetTitleColor(UIColor.FromRGB(233, 69, 141), UIControlState.Normal);
                utenteButton.BackgroundColor = UIColor.White;
                acquistiButton.SetTitleColor(UIColor.White, UIControlState.Normal);
                acquistiButton.BackgroundColor = UIColor.FromRGB(233, 69, 141);
                premiButton.SetTitleColor(UIColor.White, UIControlState.Normal);
                premiButton.BackgroundColor = UIColor.FromRGB(233, 69, 141);

                foreach (UIView sub in ContentView.Subviews)
                {
                    sub.RemoveFromSuperview();
                }

                ContentView.AddSubview(utenteView);
            }

            if (index == 1)
            {
                acquistiButton.SetTitleColor(UIColor.FromRGB(233, 69, 141), UIControlState.Normal);
                acquistiButton.BackgroundColor = UIColor.White;
                utenteButton.SetTitleColor(UIColor.White, UIControlState.Normal);
                utenteButton.BackgroundColor = UIColor.FromRGB(233, 69, 141);
                premiButton.SetTitleColor(UIColor.White, UIControlState.Normal);
                premiButton.BackgroundColor = UIColor.FromRGB(233, 69, 141);


                foreach (UIView sub in ContentView.Subviews)
                {
                    sub.RemoveFromSuperview();
                }

                ContentView.AddSubview(acquistiView);
            }

            if (index == 2)
            {
                premiButton.SetTitleColor(UIColor.FromRGB(233, 69, 141), UIControlState.Normal);
                premiButton.BackgroundColor = UIColor.White;
                acquistiButton.SetTitleColor(UIColor.White, UIControlState.Normal);
                acquistiButton.BackgroundColor = UIColor.FromRGB(233, 69, 141);
                utenteButton.SetTitleColor(UIColor.White, UIControlState.Normal);
                utenteButton.BackgroundColor = UIColor.FromRGB(233, 69, 141);


                foreach (UIView sub in ContentView.Subviews)
                {
                    sub.RemoveFromSuperview();
                }

                ContentView.AddSubview(premiView);
            }


            // Change View

            

        }
    }
}
