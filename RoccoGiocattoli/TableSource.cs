using System;
using System.Drawing;

using CoreGraphics;
using Foundation;
using UIKit;

namespace RoccoGiocattoli
{
    public class TableSource : UITableViewSource
    {
       

        public TableSource() { }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return 7;
        }



        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            Console.WriteLine(indexPath.Row);
            if (indexPath.Row == 5) {
                MenuController.Self.SendEmail();
                
            }
            if (indexPath.Row == 6)
            {
                HomePage.Self.Logout();

            }


            //base.RowSelected(tableView, indexPath);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            string cellIdentifier = @"Cell";

            switch (indexPath.Row)
            {
                case 0:
                    cellIdentifier = @"home";
                    break;

                case 1:
                    cellIdentifier = @"rocco";
                    break;

                case 2:
                    cellIdentifier = @"profilo";
                    break;

                case 3:
                    cellIdentifier = @"negozi";
                    break;

                case 4:
                    cellIdentifier = @"news";
                    break;

                case 5:
                    cellIdentifier = @"contatto";
                    break;

                case 6:
                    cellIdentifier = @"logout";
                    break;
            }

            UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);
            return cell;
        }
    }
}