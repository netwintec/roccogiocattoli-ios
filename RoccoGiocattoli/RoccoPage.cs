﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using SWRevealViewControllerBinding;
using CoreGraphics;
using MessageUI;

namespace RoccoGiocattoli
{
	partial class RoccoPage : UIViewController
	{

        int yy = 0;
        nfloat ViewWidht;
        private UIScrollView scrollView;
        UIView PrimaView, SecondaView;
        MFMailComposeViewController mailController;
        string[] to = new string[] { "info@roccogiocattoli.com" };

        public RoccoPage (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewWidht = View.Frame.Width;
            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));

            Console.WriteLine(View.Frame + "           " + View.Bounds);

            if (this.RevealViewController() == null)
                return;
            /*this.revealViewController().r = 0.0f;
            [self.revealButtonItem setTarget:self.revealViewController];
            [self.revealButtonItem setAction:@selector(rightRevealToggle: )];*/
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RightRevealToggleAnimated(true);
            //MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            /*int SystemVersion = Convert.ToInt16(UIDevice.CurrentDevice.SystemVersion.Split('.')[0]);
            if (SystemVersion >= 9)
            {
                scrollView = new UIScrollView(new CGRect(0, 64, View.Frame.Width, View.Frame.Height - 64));
            }
            else
            {
                scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height));
            }*/

            if (MFMailComposeViewController.CanSendMail)
            {
                mailController = new MFMailComposeViewController();
                mailController.SetToRecipients(to);
                mailController.Finished += (object s, MFComposeResultEventArgs args) =>
                {

                    Console.WriteLine(args.Result.ToString());

                    BeginInvokeOnMainThread(() =>
                    {
                        args.Controller.DismissViewController(true, null);
                    });
                };
            }
            bool isPhone;

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                isPhone = true;
            }
            else {
                isPhone = false;
            }

            if (isPhone)
            {
                //creazione View Ristretta
                PrimaView = new UIView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height - 64));

                int yyUtente = 0;
                UIScrollView scrollViewUtente = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height - 64));

                UILabel Nome = new UILabel(new CGRect(View.Frame.Width / 2 - 75, 10, 150, 25));
                Nome.Text = "CHI SIAMO";
                Nome.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Nome.TextAlignment = UITextAlignment.Center;
                yyUtente += 35;

                UIImageView image = new UIImageView(new CGRect(View.Frame.Width / 2 - 41, yyUtente + 3, 82, 10));
                image.Image = UIImage.FromFile("stelline_rosa.png");
                yyUtente += 11;

                UILabel Text = new UILabel(new CGRect(View.Frame.Width / 2 - 140, yyUtente + 10, 280, 70));
                Text.Text = "Rocco Giocattoli, fondata nel 1962 dal sig. Rocco D’Alessandris attuale presidente della Società, è una delle storiche realtà nella distribuzione del giocattolo in Italia. ";
                Text.Lines = 5;
                Text.Font = UIFont.BoldSystemFontOfSize(14);
                Text.TextAlignment = UITextAlignment.Center;
                UITapGestureRecognizer labelTap = new UITapGestureRecognizer(() =>
                {
                    foreach (UIView sub in ContentView.Subviews)
                    {
                        sub.RemoveFromSuperview();
                    }
                    ContentView.AddSubview(SecondaView);
                });

                Text.UserInteractionEnabled = true;
                Text.AddGestureRecognizer(labelTap);
                yyUtente += 80;

                UIImageView pallini = new UIImageView(new CGRect(View.Frame.Width / 2 - 9, yyUtente + 5, 18, 4));
                pallini.Image = UIImage.FromFile("pallini.png");
                yyUtente += 9;

                UIView LineView = new UIView(new CGRect(View.Frame.Width / 2 - 140, yyUtente + 1, 280, 1));
                LineView.BackgroundColor = UIColor.FromRGB(233, 69, 141);
                yyUtente += 2;

                UILabel Nome2 = new UILabel(new CGRect(View.Frame.Width / 2 - 115, yyUtente + 60, 230, 25));
                Nome2.Text = "UFFICI E SHOW ROOM";
                Nome2.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Nome2.TextAlignment = UITextAlignment.Center;
                yyUtente += 85;

                UIImageView image2 = new UIImageView(new CGRect(View.Frame.Width / 2 - 41, yyUtente + 1, 82, 10));
                image2.Image = UIImage.FromFile("stelline_rosa.png");
                yyUtente += 11;

                UILabel Text2 = new UILabel(new CGRect(View.Frame.Width / 2 - 150, yyUtente + 10, 300, 100));
                Text2.Text = "Via A. Carruccio 181/183 – 00134 Roma\n(Uscita 24 - Ardeatina - G.R.A.)\nTel. 06/713582 - Fax 06/71350056\nEmail: info@roccogiocattoli.com\nP.IVA 01491171003";
                Text2.Font = UIFont.BoldSystemFontOfSize(13);
                Text2.Lines = 5;
                Text2.TextAlignment = UITextAlignment.Center;
                yyUtente += 110;

                UIView ButtonView = new UIView(new CGRect(View.Frame.Width / 2 - 75, yyUtente + 20, 150, 50));
                ButtonView.BackgroundColor = UIColor.FromRGB(233, 69, 141);
                ButtonView.Layer.CornerRadius = 7;
                UITapGestureRecognizer ViewTap = new UITapGestureRecognizer(() =>
                {
                    //Console.WriteLine("Prova2");
                    if (MFMailComposeViewController.CanSendMail)
                    {
                        this.PresentViewController(mailController, true, null);
                    }
                    else
                    {
                        new UIAlertView("Mail not supported", "Can't send mail from this device", null, "OK");
                    }
                });

                ButtonView.UserInteractionEnabled = true;
                ButtonView.AddGestureRecognizer(ViewTap);
                yyUtente += 70;

                UIImageView ButtonImage = new UIImageView(new CGRect(20, 14, 30, 22));
                ButtonImage.Image = UIImage.FromFile("icona_mail.png");

                UILabel ButtonText = new UILabel(new CGRect(60, 14, 70, 22));
                ButtonText.Text = "Contattaci";
                ButtonText.TextColor = UIColor.White;
                ButtonText.Font = UIFont.BoldSystemFontOfSize(12);
                ButtonText.TextAlignment = UITextAlignment.Center;

                ButtonView.AddSubview(ButtonImage);
                ButtonView.AddSubview(ButtonText);

                scrollViewUtente.Add(Nome);
                scrollViewUtente.Add(image);
                scrollViewUtente.Add(Text);
                scrollViewUtente.Add(pallini);
                scrollViewUtente.Add(LineView);
                scrollViewUtente.Add(Nome2);
                scrollViewUtente.Add(image2);
                scrollViewUtente.Add(Text2);
                scrollViewUtente.Add(ButtonView);

                scrollViewUtente.ContentSize = new CGSize(View.Frame.Width, yyUtente + 20);


                PrimaView.Add(scrollViewUtente);

                //creazione View Intera
                SecondaView = new UIView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height - 64));

                int yyUtente2 = 0;
                UIScrollView scrollViewUtente2 = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height - 64));

                UILabel Nome3 = new UILabel(new CGRect(View.Frame.Width / 2 - 75, 10, 150, 25));
                Nome3.Text = "CHI SIAMO";
                Nome3.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Nome3.TextAlignment = UITextAlignment.Center;
                yyUtente2 += 35;

                UIImageView image3 = new UIImageView(new CGRect(View.Frame.Width / 2 - 41, yyUtente2 + 3, 82, 10));
                image3.Image = UIImage.FromFile("stelline_rosa.png");
                yyUtente2 += 11;

                UILabel Text3 = new UILabel(new CGRect(View.Frame.Width / 2 - 140, yyUtente2 + 10, 280, 420));
                Text3.Text = "Rocco Giocattoli, fondata nel 1962 dal sig. Rocco D’Alessandris attuale presidente della Società, è una delle storiche realtà nella distribuzione del giocattolo in Italia.\nOggi Rocco Giocattoli è strutturata in quattro unità di business: l’area DISTRIBUZIONE, che grazie alla presenza di oltre 20 agenti commerciali distribuisce in esclusiva prodotti e linee di importanti marchi internazionali sul territorio italiano, oltre alla distribuzione di prodotti di propria ideazione e/o produzione; l’area RETAIL, la rete di punti vendita in parte gestiti direttamente in parte in affiliazione sul territorio laziale; l’area shop-in-shop COIN, UPIM e OVS, la rete di corner specializzati all’interno dei negozi del gruppo Coin, Upim e OVS, tutti  accomunati da una medesima immagine coordinata, e pensati per rispondere al meglio alle aspettative dei consumatori; e l’area E-COMMERCE sia diretta ai consumatori finali (B2C), sia i clienti (B2B), perché consapevoli dell’estrema importanza dei canali di vendita on-line.";
                Text3.Lines = 25;
                Text3.Font = UIFont.BoldSystemFontOfSize(14);
                Text3.TextAlignment = UITextAlignment.Center;
                UITapGestureRecognizer labelTap3 = new UITapGestureRecognizer(() =>
                {
                    foreach (UIView sub in ContentView.Subviews)
                    {
                        sub.RemoveFromSuperview();
                    }
                    ContentView.AddSubview(PrimaView);
                });

                Text3.UserInteractionEnabled = true;
                Text3.AddGestureRecognizer(labelTap3);
                yyUtente2 += 430;

                UIImageView pallini3 = new UIImageView(new CGRect(View.Frame.Width / 2 - 9, yyUtente2 + 5, 18, 4));
                pallini3.Image = UIImage.FromFile("pallini.png");
                yyUtente2 += 9;

                UIView LineView3 = new UIView(new CGRect(View.Frame.Width / 2 - 140, yyUtente2 + 1, 280, 1));
                LineView3.BackgroundColor = UIColor.FromRGB(233, 69, 141);
                yyUtente2 += 2;

                UILabel Nome4 = new UILabel(new CGRect(View.Frame.Width / 2 - 115, yyUtente2 + 60, 230, 25));
                Nome4.Text = "UFFICI E SHOW ROOM";
                Nome4.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Nome4.TextAlignment = UITextAlignment.Center;
                yyUtente2 += 85;

                UIImageView image4 = new UIImageView(new CGRect(View.Frame.Width / 2 - 41, yyUtente2 + 1, 82, 10));
                image4.Image = UIImage.FromFile("stelline_rosa.png");
                yyUtente2 += 11;

                UILabel Text4 = new UILabel(new CGRect(View.Frame.Width / 2 - 150, yyUtente2 + 10, 300, 100));
                Text4.Text = "Via A. Carruccio 181/183 – 00134 Roma\n(Uscita 24 - Ardeatina - G.R.A.)\nTel. 06/713582 - Fax 06/71350056\nEmail: info@roccogiocattoli.com\nP.IVA 01491171003";
                Text4.Font = UIFont.BoldSystemFontOfSize(13);
                Text4.Lines = 5;
                Text4.TextAlignment = UITextAlignment.Center;
                yyUtente2 += 110;

                UIView ButtonView4 = new UIView(new CGRect(View.Frame.Width / 2 - 75, yyUtente2 + 20, 150, 50));
                ButtonView4.BackgroundColor = UIColor.FromRGB(233, 69, 141);
                ButtonView4.Layer.CornerRadius = 7;
                UITapGestureRecognizer ViewTap4 = new UITapGestureRecognizer(() =>
                {
                    Console.WriteLine("Prova2");
                    if (MFMailComposeViewController.CanSendMail)
                    {
                        this.PresentViewController(mailController, true, null);
                    }
                    else
                    {
                        new UIAlertView("Mail not supported", "Can't send mail from this device", null, "OK");
                    }
                });

                ButtonView4.UserInteractionEnabled = true;
                ButtonView4.AddGestureRecognizer(ViewTap4);
                yyUtente2 += 70;

                UIImageView ButtonImage4 = new UIImageView(new CGRect(20, 14, 30, 22));
                ButtonImage4.Image = UIImage.FromFile("icona_mail.png");

                UILabel ButtonText4 = new UILabel(new CGRect(60, 14, 70, 22));
                ButtonText4.Text = "Contattaci";
                ButtonText4.TextColor = UIColor.White;
                ButtonText4.Font = UIFont.BoldSystemFontOfSize(12);
                ButtonText4.TextAlignment = UITextAlignment.Center;

                ButtonView4.AddSubview(ButtonImage4);
                ButtonView4.AddSubview(ButtonText4);

                scrollViewUtente2.Add(Nome3);
                scrollViewUtente2.Add(image3);
                scrollViewUtente2.Add(Text3);
                scrollViewUtente2.Add(pallini3);
                scrollViewUtente2.Add(LineView3);
                scrollViewUtente2.Add(Nome4);
                scrollViewUtente2.Add(image4);
                scrollViewUtente2.Add(Text4);
                scrollViewUtente2.Add(ButtonView4);

                scrollViewUtente2.ContentSize = new CGSize(View.Frame.Width, yyUtente2 + 20);


                SecondaView.Add(scrollViewUtente2);


                ContentView.AddSubview(PrimaView);
            } else
            {

                PrimaView = new UIView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height - 64));

                int yyUtente = 0;
                UIScrollView scrollViewUtente = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height - 64));

                UILabel Nome = new UILabel(new CGRect(View.Frame.Width / 2 - 75, 10, 150, 25));
                Nome.Text = "CHI SIAMO";
                Nome.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Nome.TextAlignment = UITextAlignment.Center;
                yyUtente += 35;

                UIImageView image = new UIImageView(new CGRect(View.Frame.Width / 2 - 41, yyUtente + 3, 82, 10));
                image.Image = UIImage.FromFile("stelline_rosa.png");
                yyUtente += 11;

                UILabel Text = new UILabel(new CGRect(40, yyUtente + 10, View.Frame.Width - 80, 400));
                Text.Text = "Rocco Giocattoli, fondata nel 1962 dal sig. Rocco D’Alessandris attuale presidente della Società, è una delle storiche realtà nella distribuzione del giocattolo in Italia.\nOggi Rocco Giocattoli è strutturata in quattro unità di business: l’area DISTRIBUZIONE, che grazie alla presenza di oltre 20 agenti commerciali distribuisce in esclusiva prodotti e linee di importanti marchi internazionali sul territorio italiano, oltre alla distribuzione di prodotti di propria ideazione e/o produzione; l’area RETAIL, la rete di punti vendita in parte gestiti direttamente in parte in affiliazione sul territorio laziale; l’area shop-in-shop COIN, UPIM e OVS, la rete di corner specializzati all’interno dei negozi del gruppo Coin, Upim e OVS, tutti  accomunati da una medesima immagine coordinata, e pensati per rispondere al meglio alle aspettative dei consumatori; e l’area E-COMMERCE sia diretta ai consumatori finali (B2C), sia i clienti (B2B), perché consapevoli dell’estrema importanza dei canali di vendita on-line.";
                Text.Lines = 17;
                Text.Font = UIFont.BoldSystemFontOfSize(20);
                Text.TextAlignment = UITextAlignment.Center;
                yyUtente += 420;



                UILabel Nome2 = new UILabel(new CGRect(View.Frame.Width / 2 - 115, yyUtente + 60, 230, 25));
                Nome2.Text = "UFFICI E SHOW ROOM";
                Nome2.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Nome2.TextAlignment = UITextAlignment.Center;
                yyUtente += 85;

                UIImageView image2 = new UIImageView(new CGRect(View.Frame.Width / 2 - 41, yyUtente + 1, 82, 10));
                image2.Image = UIImage.FromFile("stelline_rosa.png");
                yyUtente += 11;

                UILabel Text2 = new UILabel(new CGRect(View.Frame.Width / 2 - 200, yyUtente + 10, 400, 130));
                Text2.Text = "Via A. Carruccio 181/183 – 00134 Roma\n(Uscita 24 - Ardeatina - G.R.A.)\nTel. 06/713582 - Fax 06/71350056\nEmail: info@roccogiocattoli.com\nP.IVA 01491171003";
                Text2.Font = UIFont.BoldSystemFontOfSize(18);
                Text2.Lines = 5;
                Text2.TextAlignment = UITextAlignment.Center;
                yyUtente += 140;

                UIView ButtonView = new UIView(new CGRect(View.Frame.Width / 2 - 100, yyUtente + 20, 200, 70));
                ButtonView.BackgroundColor = UIColor.FromRGB(233, 69, 141);
                ButtonView.Layer.CornerRadius = 7;
                UITapGestureRecognizer ViewTap = new UITapGestureRecognizer(() =>
                {
                    //Console.WriteLine("Prova2");
                    if (MFMailComposeViewController.CanSendMail)
                    {
                        this.PresentViewController(mailController, true, null);
                    }
                    else
                    {
                        new UIAlertView("Mail not supported", "Can't send mail from this device", null, "OK");
                    }
                });

                ButtonView.UserInteractionEnabled = true;
                ButtonView.AddGestureRecognizer(ViewTap);
                yyUtente += 90;

                UIImageView ButtonImage = new UIImageView(new CGRect(30, 20, 40, 30));
                ButtonImage.Image = UIImage.FromFile("icona_mail.png");

                UILabel ButtonText = new UILabel(new CGRect(80, 20, 90, 30));
                ButtonText.Text = "Contattaci";
                ButtonText.TextColor = UIColor.White;
                ButtonText.Font = UIFont.BoldSystemFontOfSize(17);
                ButtonText.TextAlignment = UITextAlignment.Center;

                ButtonView.AddSubview(ButtonImage);
                ButtonView.AddSubview(ButtonText);

                scrollViewUtente.Add(Nome);
                scrollViewUtente.Add(image);
                scrollViewUtente.Add(Text);
                scrollViewUtente.Add(Nome2);
                scrollViewUtente.Add(image2);
                scrollViewUtente.Add(Text2);
                scrollViewUtente.Add(ButtonView);

                scrollViewUtente.ContentSize = new CGSize(View.Frame.Width, yyUtente + 20);


                PrimaView.Add(scrollViewUtente);

                ContentView.AddSubview(PrimaView);
            }
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }
}
