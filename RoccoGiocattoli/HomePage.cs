﻿using System;
using System.Json;
using Newtonsoft.Json.Linq;
using UIKit;
using SWRevealViewControllerBinding;
using Foundation;
using CoreGraphics;
using Carousels;
using System.Linq;
using RestSharp;
using System.Xml;
using System.Threading;
using System.Text;
using Facebook.LoginKit;
using System.Threading.Tasks;

namespace RoccoGiocattoli
{
    public partial class HomePage : UIViewController
    {

        int yy = 0;
		int porta =81;
        nfloat ViewWidht;
        private UIScrollView scrollView;
        public iCarousel carousel;
        bool isPhone;
        double Widht, Height, Px, Py;

        public HomePage(IntPtr handle)
          : base(handle)
        { }

        public static HomePage Self { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewWidht = View.Frame.Width;
            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));
            HomePage.Self = this;

            if(!AppDelegate.Instance.BluetoothAlert && AppDelegate.Instance.BluetoothOff)
            {
                var alert = new UIAlertView
                {
                    Title = "Incredibili Promozioni",
                    Message = "Accendi il Bluetooth e cerca le incredibili promozioni in negozio.",  
                };
                alert.AddButton("Ok");
                alert.Show();
                AppDelegate.Instance.BluetoothAlert = true;
            }
            

            if (AppDelegate.Instance.XmlObjectDictionary.Count == 0)
            {
                var client = new RestClient("http://shop.roccogiocattoli.eu/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                var requestN4U = new RestRequest("xml/homepage.php", Method.GET);
                requestN4U.AddHeader("content-type", "application/xml");

                IRestResponse response = client.Execute(requestN4U);

                try {
                    string content = response.Content;
                    Console.WriteLine("Request:" + content);
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(content);
                    int i = 0;
                    foreach (XmlElement xe in xmldoc.SelectNodes("products/item"))
                    {

                        string nome = xe.SelectSingleNode("name").InnerText;
                        string url = xe.SelectSingleNode("url").InnerText;
                        string image = xe.SelectSingleNode("image").InnerText;

                        AppDelegate.Instance.XmlObjectDictionary.Add(i, new XmlObject(nome, url, image));
                        i++;
                    }
                    AppDelegate.Instance.totalValue = i;
                }
                catch (Exception e) {
                    AppDelegate.Instance.XmlObjectDictionary.Add(0, new XmlObject("Errore Dati Non Disponibili", null, null));
                    AppDelegate.Instance.totalValue = 1;
                }
            }

            AppDelegate.Instance.Scan = true;
            AppDelegate.Instance.TokenN4U = NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U");
            
            //******** INVIO TOKEN PUSH *******
            try
            {
                var clientPush = new RestClient("http://api.netwintec.com:" + porta + "/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			    var requestN4UPush = new RestRequest("notification", Method.POST);
			    requestN4UPush.AddHeader("content-type", "application/json");
			    requestN4UPush.AddHeader("Net4U-Company", "roccogiocattoli");
                requestN4UPush.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));
                requestN4UPush.Timeout = 60000;

			    JObject oJsonObject = new JObject();

			    oJsonObject.Add("uuid", AppDelegate.Instance.PushToken);
			    oJsonObject.Add("type", "ios");

			    requestN4UPush.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                if(AppDelegate.Instance.PushToken!=null)
                    clientPush.ExecuteAsync(requestN4UPush, null);

            }catch(Exception e)
            {
                Console.WriteLine("Aaaa");
            }
			//************************


            UIStoryboard storyboard = new UIStoryboard();

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                isPhone = true;
                Widht = (View.Frame.Width - 60) / 2;
                Height = Widht;
                Px = 20;
                Py = 20;
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                isPhone = false;
                Widht = (View.Frame.Width - 240) / 2;
                Height = Widht;
                Px = 80;
                Py = 80;
            }
            UIViewController rocco = storyboard.InstantiateViewController("RoccoController");
            UIViewController profilo = storyboard.InstantiateViewController("ProfiloController");
            UIViewController news = storyboard.InstantiateViewController("NewsController");
            UIViewController negozi = storyboard.InstantiateViewController("NegoziController");

            if (this.RevealViewController() == null)
                return;
            /*this.revealViewController().r = 0.0f;
            [self.revealButtonItem setTarget:self.revealViewController];
            [self.revealButtonItem setAction:@selector(rightRevealToggle: )];*/
            this.RevealViewController().RightViewRevealOverdraw=0.0f;



            MenuItem.Clicked += (sender, e) => this.RevealViewController().RightRevealToggleAnimated(true);
            //LogoutItem.Clicked += (sender, e) => Logout();
            //MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height));
            


            UIButton roccoButton = new UIButton(new CGRect(Px, Py, Widht, Height));
            Console.WriteLine(roccoButton.Frame);
            UIImageView roccoImage = new UIImageView(roccoButton.Bounds);
            roccoImage.Image = UIImage.FromFile("rocco_icon.png");
            roccoButton.AddSubview(roccoImage);
            roccoButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(rocco, true);

             
            };



            UIButton profiloButton = new UIButton(new CGRect(Widht+2*Px, Py, Widht, Height));
            Console.WriteLine(profiloButton.Frame);
            UIImageView profiloImage = new UIImageView(profiloButton.Bounds);
            profiloImage.Image = UIImage.FromFile("profilo_icon.png");
            profiloButton.AddSubview(profiloImage);
            profiloButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(profilo, true);
            };

            yy += (int)(Height+Py);


            UIButton newsButton = new UIButton(new CGRect(Px, yy+Py, Widht,Height));
            Console.WriteLine(newsButton.Frame);
            UIImageView newsImage = new UIImageView(newsButton.Bounds);
            newsImage.Image = UIImage.FromFile("news_icon.png");
            newsButton.AddSubview(newsImage);
            newsButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(news, true);
            };

            UIButton negoziButton = new UIButton(new CGRect(Widht + 2 * Px, yy+Py, Widht, Height));
            Console.WriteLine(negoziButton.Frame);
            UIImageView negoziImage = new UIImageView(negoziButton.Bounds);
            negoziImage.Image = UIImage.FromFile("negozi_icon.png");
            negoziButton.AddSubview(negoziImage);
            negoziButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(negozi, true);
            };

            yy += (int)(Height + Py);

            UILabel Label;
            if (isPhone)
            {
                Label = new UILabel(new CGRect(ViewWidht / 2 - 100, yy + 20, 200, 25));
                Label.Text = "Novità dallo Shop";
                Label.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Label.TextAlignment = UITextAlignment.Center;
                yy += 45;
            }
            else
            {
                Label = new UILabel(new CGRect(ViewWidht / 2 - 200, yy + 20, 400, 35));
                byte[] utf8bytes = Encoding.UTF8.GetBytes("Novità dallo Shop");
                string r = Encoding.UTF8.GetString(utf8bytes, 0, utf8bytes.Length);
                Label.Text = r;
                Label.Font = UIFont.FromName("BubblegumSans-Regular", 30);
                Label.TextAlignment = UITextAlignment.Center;
                yy += 55;
            }

            UIImageView imageView = new UIImageView(new CGRect(ViewWidht / 2 - 65, yy + 5, 130, 15));
            imageView.Image = UIImage.FromFile("stelline_rosa.png");
            yy += 23;



            if (isPhone)
            {
                carousel = new iCarousel(new CGRect(ViewWidht / 2 - 100, yy + 20, 200, 250));
                carousel.Type = iCarouselType.CoverFlow2;
                carousel.DataSource = new CarouselDataSource(AppDelegate.Instance.totalValue);
                carousel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
                yy += 270;
            }
            else
            {
                carousel = new iCarousel(new CGRect(ViewWidht / 2 - 200, yy + 40, 400, 450));
                carousel.Type = iCarouselType.CoverFlow2;
                carousel.DataSource = new CarouselDataSource(AppDelegate.Instance.totalValue);
                carousel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
                yy += 470;
            }


            // handle item selections / taps
            carousel.ItemSelected += (sender, args) => {
                var indexSelected = args.Index;
                Console.WriteLine(indexSelected);
                UIApplication.SharedApplication.OpenUrl(new NSUrl(AppDelegate.Instance.XmlObjectDictionary[(int)indexSelected].Url));
                // do something with a selection
            };

            

            scrollView.Add(roccoButton);
            scrollView.Add(profiloButton);
            scrollView.Add(newsButton);
            scrollView.Add(negoziButton);
            scrollView.Add(Label);
            scrollView.Add(imageView);
            scrollView.Add(carousel);


            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 20);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            ContentPage.Add(scrollView);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Logout()
        {
            UIStoryboard storyboard;
            LoginManager loginManager = new LoginManager();
            int button = await ShowAlert("Logout", "Vuoi effettuare il Logout", "Ok", "Annulla");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {
                loginManager.LogOut();

                //******** DELETE TOKEN PUSH *******
                try
                {
                    var clientPush = new RestClient("http://api.netwintec.com:" + porta + "/");
                    //client.Authenticator = new HttpBasicAuthenticator(username, password);

                    var requestN4UPush = new RestRequest("notification", Method.DELETE);
                    requestN4UPush.AddHeader("content-type", "application/json");
                    requestN4UPush.AddHeader("Net4U-Company", "roccogiocattoli");
                    requestN4UPush.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));
                    requestN4UPush.Timeout = 60000;

                    JObject oJsonObject = new JObject();

                    oJsonObject.Add("uuid", AppDelegate.Instance.PushToken);
                    oJsonObject.Add("type", "ios");

                    requestN4UPush.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                    if (AppDelegate.Instance.PushToken != null)
                        clientPush.ExecuteAsync(requestN4UPush, (s,e) => {

                            Console.WriteLine("Logout"+s.Content+"!"+s.StatusCode);

                        });
                }
                catch (Exception e)
                {
                    Console.WriteLine("Aaaa");
                }
                //************************

                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }
                NSUserDefaults.StandardUserDefaults.SetString("", "TokenRoccoN4U");
                UIViewController lp = storyboard.InstantiateViewController("RootView");
                AppDelegate.Instance.flagTokenN4U = true;
                AppDelegate.Instance.Scan = false;
                AppDelegate.Instance.TokenN4U = "";
                this.PresentModalViewController(lp, true);
            }
            else { }
        }

        private class CarouselDataSource : iCarouselDataSource
        {
            int[] items;

            public CarouselDataSource(int number)
            {
                // create our amazing data source
                items = Enumerable.Range(number, number).ToArray();
            }

            // let the carousel know how many items to render
            public override nint GetNumberOfItems(iCarousel carousel)
            {
                // return the number of items in the data
                return items.Length;
            }


            // create the view each item in the carousel
            public override UIView GetViewForItem(iCarousel carousel, nint index, UIView view)
            {
                UILabel label = null;
                UIImageView imageView = null;
                UIView View = null;

                if (view == null)
                {
                    if (HomePage.Self.isPhone)
                    {
                        View = new UIView(new CGRect(0, 0, 200, 250));

                        UIImage Placeholder = UIImage.FromFile("placeholder.png");

                        imageView = new UIImageView(new CGRect(0, 0, 200.0f, 200.0f));
                        imageView.Image = Placeholder;
                        imageView.Tag = 2;

                        UIImage cachedImage = AppDelegate.Instance.XmlObjectDictionary[(int)index].UIimage;

                        if (cachedImage == null)
                        {

                            ThreadPool.QueueUserWorkItem((t) =>
                            {

                                // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                                //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                                if (AppDelegate.Instance.XmlObjectDictionary[(int)index].Image == null)
                                {
                                    cachedImage = Placeholder;
                                }
                                else
                                {
                                    cachedImage = FromUrl(AppDelegate.Instance.XmlObjectDictionary[(int)index].Image) ?? Placeholder;
                                }

                                InvokeOnMainThread(() =>
                                {
                                    imageView.Image = cachedImage;

                                    AppDelegate.Instance.XmlObjectDictionary[(int)index].UIimage = cachedImage;
                                });

                            });

                        }
                        else
                        {
                            imageView.Image = cachedImage;
                        }

                        label = new UILabel(new CGRect(0, 200, 200, 50));
                        label.Text = (AppDelegate.Instance.XmlObjectDictionary[(int)index].Nome);
                        label.BackgroundColor = UIColor.Clear;
                        label.TextAlignment = UITextAlignment.Center;
                        label.Font = label.Font.WithSize(13);
                        label.Lines = 3;
                        label.Tag = 1;

                        View.AddSubview(imageView);
                        View.AddSubview(label);
                    } else {
                        View = new UIView(new CGRect(0, 0, 400, 450));

                        UIImage Placeholder = UIImage.FromFile("placeholder.png");

                        imageView = new UIImageView(new CGRect(0, 0, 400.0f, 400.0f));
                        imageView.Image = Placeholder;
                        imageView.Tag = 2;

                        UIImage cachedImage = AppDelegate.Instance.XmlObjectDictionary[(int)index].UIimage;

                        if (cachedImage == null)
                        {

                            ThreadPool.QueueUserWorkItem((t) =>
                            {

                                // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                                //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                                cachedImage = FromUrl(AppDelegate.Instance.XmlObjectDictionary[(int)index].Image) ?? Placeholder;

                                InvokeOnMainThread(() =>
                                {
                                    imageView.Image = cachedImage;

                                    AppDelegate.Instance.XmlObjectDictionary[(int)index].UIimage = cachedImage;
                                });

                            });

                        }
                        else
                        {
                            imageView.Image = cachedImage;
                        }

                        label = new UILabel(new CGRect(0, 400,400, 50));
                        label.Text = (AppDelegate.Instance.XmlObjectDictionary[(int)index].Nome);
                        label.BackgroundColor = UIColor.Clear;
                        label.TextAlignment = UITextAlignment.Center;
                        label.Font = label.Font.WithSize(13);
                        label.Lines = 2;
                        label.Tag = 1;

                        View.AddSubview(imageView);
                        View.AddSubview(label);
                    }
                }
                else
                {
                    // get a reference to the label in the recycled view
                    View = view;
                    imageView = (UIImageView)view.ViewWithTag(2);
                    label = (UILabel)view.ViewWithTag(1);

                    
                    //label = (UILabel)view.ViewWithTag(1);
                }

                // set the values of the view

                return View;
            }
        }

        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }


    }
}
