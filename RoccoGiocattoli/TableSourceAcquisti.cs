﻿using System;
using System.Drawing;

using CoreGraphics;
using Foundation;
using UIKit;
using System.Collections.Generic;

namespace RoccoGiocattoli
{
    public class TableSourceAcquisti : UITableViewSource
    {

        public List<AcquistiObject> ListAquistiObject = new List<AcquistiObject>();
        int righe;
        int selected;
        bool descAp=false;
        bool isPhone;

        public TableSourceAcquisti(List<AcquistiObject> list, bool phone ) {
            ListAquistiObject = list;
            righe = ListAquistiObject.Count;
            selected = ListAquistiObject.Count + 10;
            isPhone = phone;
            
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return righe;
        }



        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            
            if (indexPath.Row == selected)
            {
                selected = ListAquistiObject.Count+10;
                righe = ListAquistiObject.Count;
                descAp = false;
            }
            else
            {
                if (indexPath.Row < selected)
                {
                    selected = indexPath.Row;
                    righe = ListAquistiObject.Count + 1;
                    descAp = true;
                }
                if (indexPath.Row > selected+1)
                {
                    selected = indexPath.Row-1;
                    righe = ListAquistiObject.Count + 1;
                    descAp = true;
                }
            }

            tableView.ReloadData();
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            if (isPhone)
            {
                int row = indexPath.Row;
                if (row == selected + 1)
                {
                    nfloat height = 70;
                    return height;
                }

                nfloat height2 = 44;
                return height2;
            }
            else
            {
                int row = indexPath.Row;
                if (row == selected + 1)
                {
                    nfloat height = 100;
                    return height;
                }

                nfloat height2 = 66;
                return height2;
            }
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            
            int row = indexPath.Row;
            if (row == selected + 1 && descAp == true)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellDettagli cell = new CustomCellDettagli(cellIdentifier,isPhone);
                
                cell.UpdateCell(ListAquistiObject[indexPath.Row-1].Data
                                , ListAquistiObject[indexPath.Row-1].PV);
                return cell;
            }
            else
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellAcquisto cell = new CustomCellAcquisto(cellIdentifier, isPhone);
                if (row < selected)
                {
                    cell.UpdateCell(ListAquistiObject[indexPath.Row].Nome
                                    , ListAquistiObject[indexPath.Row].Prezzo
                                    , UIImage.FromFile("freccia_giu.png"));
                    
                }
                if (row == selected)
                {
                    cell.UpdateCell(ListAquistiObject[indexPath.Row].Nome
                                    , ListAquistiObject[indexPath.Row].Prezzo
                                    , UIImage.FromFile("freccia_su.png"));
                    
                }
                if (row > selected)
                {
                    cell.UpdateCell(ListAquistiObject[indexPath.Row-1].Nome
                                    , ListAquistiObject[indexPath.Row-1].Prezzo
                                    , UIImage.FromFile("freccia_giu.png"));
                    
                }

                return cell;

            }
            
        }
    }
}
