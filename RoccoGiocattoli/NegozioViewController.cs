using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using SWRevealViewControllerBinding;
using CoreAnimation;

namespace RoccoGiocattoli
{
	partial class NegozioViewController : UIViewController
	{
        bool flag = false;

		public NegozioViewController (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (this.RevealViewController() == null)
                return;


            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));

            var url = "http://www.roccogiocattoli.eu/punti-vendita-app/"; // NOTE: https secure request

            CABasicAnimation rotationAnimation = new CABasicAnimation();
            rotationAnimation.KeyPath = "transform.rotation.z";
            rotationAnimation.To = new NSNumber(Math.PI * 2);
            rotationAnimation.Duration = 2;
            rotationAnimation.Cumulative = true;
            rotationAnimation.RepeatCount = float.MaxValue;
            LoadingImage.Layer.AddAnimation(rotationAnimation, "rotationAnimation");

            
            webView.LoadFinished += delegate {
                if (flag == false)
                {
                    webView.Alpha = 1;
                    LoadingImage.Layer.RemoveAllAnimations();
                    LoadingImage.Alpha = 0;
                    flag = true;

                }
                //Console.WriteLine("Finish");
            };
            webView.LoadRequest(new NSUrlRequest(new NSUrl(url)));

            /*this.revealViewController().r = 0.0f;
            [self.revealButtonItem setTarget:self.revealViewController];
            [self.revealButtonItem setAction:@selector(rightRevealToggle: )];*/
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RightRevealToggleAnimated(true);
            //MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

    }
}
