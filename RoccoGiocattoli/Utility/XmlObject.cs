﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UIKit;

namespace RoccoGiocattoli
{		
	public class XmlObject 
	{
		public string Nome,Url,Image;
		public UIImage UIimage;

		public XmlObject(string nome,string url,string image){
		
			Nome = nome;
			Url = url;
			Image = image;
		
		}

	}
}

