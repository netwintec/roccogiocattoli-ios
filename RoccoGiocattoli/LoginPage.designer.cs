// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RoccoGiocattoli
{
	[Register ("LoginPage")]
	partial class LoginPage
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel AccediText { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel CondizioniLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView EmailBorder { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField EmailText { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView FacebookView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView LoadingImage { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView LoadView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView MainScrollView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton NormalLoginButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView PasswordBorder { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField PasswordText { get; set; }

		[Action ("NormalLoginButton_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void NormalLoginButton_TouchUpInside (UIButton sender);

		[Action ("PasswordLost:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void PasswordLost (UIButton sender);

		[Action ("PasswordLostIpad:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void PasswordLostIpad (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (AccediText != null) {
				AccediText.Dispose ();
				AccediText = null;
			}
			if (CondizioniLabel != null) {
				CondizioniLabel.Dispose ();
				CondizioniLabel = null;
			}
			if (EmailBorder != null) {
				EmailBorder.Dispose ();
				EmailBorder = null;
			}
			if (EmailText != null) {
				EmailText.Dispose ();
				EmailText = null;
			}
			if (FacebookView != null) {
				FacebookView.Dispose ();
				FacebookView = null;
			}
			if (LoadingImage != null) {
				LoadingImage.Dispose ();
				LoadingImage = null;
			}
			if (LoadView != null) {
				LoadView.Dispose ();
				LoadView = null;
			}
			if (MainScrollView != null) {
				MainScrollView.Dispose ();
				MainScrollView = null;
			}
			if (NormalLoginButton != null) {
				NormalLoginButton.Dispose ();
				NormalLoginButton = null;
			}
			if (PasswordBorder != null) {
				PasswordBorder.Dispose ();
				PasswordBorder = null;
			}
			if (PasswordText != null) {
				PasswordText.Dispose ();
				PasswordText = null;
			}
		}
	}
}
