﻿using CoreGraphics;
using Facebook.LoginKit;
using Foundation;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Threading.Tasks;
using UIKit;
using ZXing.Mobile;

namespace RoccoGiocattoli
{
	partial class CardPage : UIViewController
	{

        UIScrollView scrollView;
        bool isPhone;
        MobileBarcodeScanner scanner;
        double Px,Padding1,Padding2,Padding3;
        double getH, getW, BH, BW, addH, addW;
        int porta = 81;

        public CardPage (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
           
            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));

            

            scanner = new MobileBarcodeScanner(this);

            UIStoryboard storyboard = new UIStoryboard();

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                isPhone = true;

                getH = 53;
                getW = 80;
                BW = 250;
                BH = 50;
                addH = 64;
                addW = 80;

                Padding1 = (10 * View.Frame.Height) / 480;
                Padding2 = (15 * View.Frame.Height) / 480;
                Padding3 = (4 * View.Frame.Height) / 480;
                Px = (View.Frame.Height - (53+Padding1+50+Padding2+3+Padding2+64+Padding3+50+86) - 64) / 3;
                Console.WriteLine(Padding1 +" "+Padding2+"  "+Padding3+" "+Px+" "+(86+53 + Padding1 + 50 + Padding2 + 3 + Padding2 + 64 + Padding3 + 50));
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                isPhone = false;
                Px = 80;

                getH = 93;
                getW = 140;
                BW = 350;
                BH = 70;
                addH = 112;
                addW = 140;


                Padding1 = (10 * View.Frame.Height) / 480;
                Padding2 = (15 * View.Frame.Height) / 480;
                Padding3 = (4 * View.Frame.Height) / 480;
                Px = (View.Frame.Height - (getH + Padding1 + BH + Padding2 + 3 + Padding2 + addH + Padding3 + BH + 86) - 64) / 3;
                Console.WriteLine(Padding1 + " " + Padding2 + "  " + Padding3 + " " + Px + " " + (86 + 53 + Padding1 + 50 + Padding2 + 3 + Padding2 + 64 + Padding3 + 50));

            }
            UIViewController home = storyboard.InstantiateViewController("RWController");

            int yy = 0;

            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height - 64));

            UIImageView imageView = new UIImageView(new CGRect(View.Frame.Width / 2 - 100, yy + Px, 200, 86));
            imageView.Image = UIImage.FromFile("logo_rocco.png");

            yy += 86+(int)Px;

            UIImageView GetCardImage = new UIImageView(new CGRect(View.Frame.Width / 2 - ((BW / 2) -5), yy + Px, getW, getH));
            GetCardImage.Image = UIImage.FromFile("get_card.png");

            UILabel LabelGet;
            if (isPhone)
            {
                LabelGet = new UILabel(new CGRect(View.Frame.Width / 2 - 35, yy + Px, 160, getH));
                LabelGet.Text = "Possiedi già\nla carta fedelta?";
                LabelGet.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                LabelGet.TextAlignment = UITextAlignment.Left;
                LabelGet.Lines = 2;
                yy += (int)getH + (int)Px;
            }
            else
            {
                LabelGet = new UILabel(new CGRect(View.Frame.Width / 2 - 15, yy + Px, 185, getH));
                LabelGet.Text = "Possiedi già\nla carta fedelta?";
                LabelGet.Font = UIFont.FromName("BubblegumSans-Regular", 28);
                LabelGet.TextAlignment = UITextAlignment.Left;
                LabelGet.Lines = 2;
                yy += (int)getH + (int)Px;
            }

            UIButton BottoneScan = new UIButton(new CGRect(View.Frame.Width /2 - (BW/2), yy + Padding1, BW, BH));
            BottoneScan.SetTitle("Scansionala", UIControlState.Normal);
            BottoneScan.BackgroundColor = UIColor.FromRGB(233, 69, 141);
            BottoneScan.Layer.CornerRadius = 7;
            BottoneScan.TouchUpInside += async delegate
            {
                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                scanner.BottomText = "Attendi che il barcode venga scansionato!";

                //Start scanning
                var result = await scanner.Scan(options);

                HandleScanResult(result);
            };

            if (!isPhone)
            {

                BottoneScan.Font = UIFont.SystemFontOfSize(22);
            }

            yy += (int)BH + (int)Padding1;

            UIView separator;
            if (isPhone)
            {
                separator = new UIView(new CGRect(30, yy + Padding2, View.Frame.Width - 60, 3));
                separator.BackgroundColor = UIColor.FromRGB(83, 83, 83);
            }
            else
            {
                separator = new UIView(new CGRect(60, yy + Padding2, View.Frame.Width - 120, 3));
                separator.BackgroundColor = UIColor.FromRGB(83, 83, 83);
            }


            yy += 3 + (int)Padding2;

            UIImageView AddCardImage = new UIImageView(new CGRect(View.Frame.Width / 2 - ((BW/2)-5), yy + Padding2, addW, addH));
            AddCardImage.Image = UIImage.FromFile("add_card.png");

            UILabel LabelAdd;
            if (isPhone)
            {
                LabelAdd = new UILabel(new CGRect(View.Frame.Width / 2 - 35, yy + Padding2, 160, addH));
                LabelAdd.Text = "Non possiedi\nla carta fedelta?";
                LabelAdd.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                LabelAdd.TextAlignment = UITextAlignment.Left;
                LabelAdd.Lines = 2;
                yy += (int)addH + (int)Padding2;
            }
            else
            {
                LabelAdd = new UILabel(new CGRect(View.Frame.Width / 2 - 15, yy + Padding2, 185, addH));
                LabelAdd.Text = "Non possiedi\nla carta fedelta?";
                LabelAdd.Font = UIFont.FromName("BubblegumSans-Regular", 28);
                LabelAdd.TextAlignment = UITextAlignment.Left;
                LabelAdd.Lines = 2;
                yy += (int)addH + (int)Padding2;
            }

            UIButton BottoneGenera = new UIButton(new CGRect(View.Frame.Width /2 - (BW / 2), yy + Padding3, BW, BH));
            BottoneGenera.SetTitle("Generala", UIControlState.Normal);
            BottoneGenera.BackgroundColor = UIColor.FromRGB(233, 69, 141);
            BottoneGenera.Layer.CornerRadius = 7;
            BottoneGenera.TouchUpInside += (object sender, EventArgs e) => {

                var client2 = new RestClient("http://api.netwintec.com:" + porta + "/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                var requestN4U2 = new RestRequest("card", Method.GET);
                requestN4U2.AddHeader("content-type", "application/json");
                requestN4U2.AddHeader("Net4U-Company", "roccogiocattoli");
                requestN4U2.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("AppoggioTokenRoccoN4U"));

                IRestResponse response2 = client2.Execute(requestN4U2);

                Console.WriteLine(response2.StatusCode + "   " + response2.Content);

                if (response2.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    UIStoryboard storyboard2;
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("MainStoryboard_iPad", null);
                    }

                    NSUserDefaults.StandardUserDefaults.SetString(NSUserDefaults.StandardUserDefaults.StringForKey("AppoggioTokenRoccoN4U"), "TokenRoccoN4U");
                    UIViewController lp;
                    lp = storyboard.InstantiateViewController("RWController");
                    this.PresentModalViewController(lp, true);
                }

                else
                {
                    Errore();
                }


            };

            if (!isPhone)
            {

                BottoneGenera.Font = UIFont.SystemFontOfSize(22);
            }

            yy += (int)BH+(int)Padding3;

            scrollView.Add(imageView);
            scrollView.Add(GetCardImage);
            scrollView.Add(LabelGet);
            scrollView.Add(BottoneScan);
            scrollView.Add(separator);
            scrollView.Add(AddCardImage);
            scrollView.Add(LabelAdd);
            scrollView.Add(BottoneGenera);

            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + Px);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            Content.Add(scrollView);
        }

        private void BottoneGenera_TouchUpInside(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void SetCard (string code){

            Console.WriteLine("scan"+code);
            var client = new RestClient("http://api.netwintec.com:" + porta + "/");
            //client.Authenticator = new HttpBasicAuthenticator(username, password);

            var requestN4U = new RestRequest("card", Method.POST);
            requestN4U.AddHeader("content-type", "application/json");
            requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
            requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("AppoggioTokenRoccoN4U"));

            

            JObject oJsonObject = new JObject();

            oJsonObject.Add("tessera", code);

            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


            IRestResponse response = client.Execute(requestN4U);

            Console.WriteLine(response.Content+"  "+ response.StatusCode);


            if (response.StatusCode== System.Net.HttpStatusCode.OK)
            {
                UIStoryboard storyboard;
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }

                NSUserDefaults.StandardUserDefaults.SetString(NSUserDefaults.StandardUserDefaults.StringForKey("AppoggioTokenRoccoN4U"), "TokenRoccoN4U");
                UIViewController lp;
                lp = storyboard.InstantiateViewController("RWController");
                this.PresentModalViewController(lp, true);
            }

            else
            {
                Errore();
            }
        }

        public async void Errore()
        {
            UIStoryboard storyboard;
            LoginManager loginManager = new LoginManager();
            int button = await ShowAlert("Errore di rete", "Assegnazione Card non riuscita.\nTornare al Menù?", "Ok", "Riprova");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {
                loginManager.LogOut();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }
                NSUserDefaults.StandardUserDefaults.SetString("", "TokenRoccoN4U");
                UIViewController lp = storyboard.InstantiateViewController("RootView");
                this.PresentModalViewController(lp, true);
            }
            else { }
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        void HandleScanResult(ZXing.Result result)
        {
            string msg = "";

            this.InvokeOnMainThread(() =>
            {
                if (result != null && !string.IsNullOrEmpty(result.Text))
                    SetCard(result.Text);
                else
                {
                    msg = "Scansione Interrotta";
                    var av = new UIAlertView("Scanner", msg, null, "OK", null);
                    av.Show();
                }
            });
        }
    }
}
