// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RoccoGiocattoli
{
	[Register ("NewsViewController")]
	partial class NewsViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView LoadingImage { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIBarButtonItem MenuItem { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIWebView webView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (LoadingImage != null) {
				LoadingImage.Dispose ();
				LoadingImage = null;
			}
			if (MenuItem != null) {
				MenuItem.Dispose ();
				MenuItem = null;
			}
			if (webView != null) {
				webView.Dispose ();
				webView = null;
			}
		}
	}
}
