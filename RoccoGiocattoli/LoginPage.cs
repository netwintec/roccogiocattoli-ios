using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using Facebook.LoginKit;
using Facebook.CoreKit;
using CoreGraphics;
using System.Collections.Generic;
using CoreAnimation;

namespace RoccoGiocattoli
{
    partial class LoginPage : UIViewController
    {
        LoginButton loginFbButton;
        List<string> readPermissions = new List<string> { "public_profile","email" };
        bool flagEMail, flagPassword;
        LoginManager loginManager;

        public LoginPage(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));

            EmailBorder.Layer.CornerRadius = 7;
            PasswordBorder.Layer.CornerRadius = 7;

            LoadView.Alpha = 0;
            CABasicAnimation rotationAnimation = new CABasicAnimation();
            rotationAnimation.KeyPath = "transform.rotation.z";
            rotationAnimation.To = new NSNumber(Math.PI * 2);
            rotationAnimation.Duration = 2;
            rotationAnimation.Cumulative = true;
            rotationAnimation.RepeatCount = float.MaxValue;
            LoadingImage.Layer.AddAnimation(rotationAnimation, "rotationAnimation");


            Profile.Notifications.ObserveDidChange((sender, e) =>
            {

                if (e.NewProfile == null)
                    return;

                //nameLabel.Text = e.NewProfile.Name;
            });

            // Set the Read and Publish permissions you want to get
            loginFbButton = new LoginButton(new CGRect(0, 0, 120, 40))//AccediText.Frame.X+(AccediText.Frame.Width/2) - 60, AccediText.Frame.Y, 120, 40))
            {
                LoginBehavior = LoginBehavior.Native,
                ReadPermissions = readPermissions.ToArray()
                
            };

            loginManager = new LoginManager();

            

            // Handle actions once the user is logged in
            loginFbButton.Completed += (sender, e) =>
            {
                if (e.Error != null)
                {
                    var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                    adErr.Show();
                    // Handle if there was an error
                }

                if (e.Result.IsCancelled)
                {
                    // Handle if the user cancelled the login request
                }

                if (e.Result.Token.TokenString != null)
                {
                    LoadView.Alpha = 1;
                    LoadView.SetNeedsLayout();
                    LoginFB(e.Result.Token.TokenString);
                }

                // Handle your successful login
            };

            // Handle actions once the user is logged out
            loginFbButton.LoggedOut += (sender, e) =>
            {
                // Handle your logout
                // nameLabel.Text = "";
            };


            NormalLoginButton.Layer.CornerRadius = 7;


            // Add views to main view
            FacebookView.AddSubview(loginFbButton);


            Console.WriteLine(base.View.Frame + " | " + base.View.Bounds);
            //MainScrollView.Frame.Height = 550;

            EmailText.ShouldReturn += (UITextField) =>
            {
                flagEMail = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            PasswordText.ShouldReturn += (UITextField) =>
            {
                flagPassword = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            EmailText.ShouldBeginEditing = delegate
            {
                flagEMail = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -115;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -25;
                    View.Frame = frame;
                }

                return true;
            };

            PasswordText.ShouldBeginEditing = delegate
            {
                flagPassword = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -125;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -35;
                    View.Frame = frame;
                }

                return true;
            };

            EmailText.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagEMail == false)
                    View.Frame = frame;
                return true;
            };

            PasswordText.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagPassword == false)
                    View.Frame = frame;
                return true;
            };

            NSObject _notification = UIKeyboard.Notifications.ObserveWillShow((s, e) =>
            {
                var r = UIKeyboard.FrameBeginFromNotification(e.Notification);
                //var keyboardHeight = r.Height;
                Console.WriteLine("Height:" + r.Height);
            });


        }


        partial void NormalLoginButton_TouchUpInside(UIButton sender)
        {
            
            LoadView.Alpha = 1;
            LoadView.SetNeedsLayout();

            UtilityLoginManager login = new UtilityLoginManager();
            List<string> result = new List<string>();
            result = login.Login(EmailText.Text, PasswordText.Text);

            if (result[0] == "SUCCESS")
            {
                Console.WriteLine(result[0] + "  " + result[1] + "  " + result[2] + "  " + result[3] + "  \"" + result[4] + "\"");

                NSUserDefaults.StandardUserDefaults.SetString(result[2], "NomeRoccoN4U");
                NSUserDefaults.StandardUserDefaults.SetString(result[3], "CognomeRoccoN4U");
                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }

                UIViewController lp;

                if (result[4] == "")
                {
                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "AppoggioTokenRoccoN4U");
                    lp = storyboard.InstantiateViewController("CardPage");
                }
                else
                {
                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "TokenRoccoN4U");
                    lp = storyboard.InstantiateViewController("RWController");
                }

                base.NavigationController.PresentViewController(lp, true, null);
            }

            if (result[0] == "ERROR")
            {
                LoadView.Alpha = 0;
                LoadView.SetNeedsLayout();
                EmailText.Text = "";
                PasswordText.Text = "";
                var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                adErr.Show();

            }

            if (result[0] == "ERRORDATA")
            {
                LoadView.Alpha = 0;
                LoadView.SetNeedsLayout();
                EmailText.Text = "";
                PasswordText.Text = "";
                var adErr = new UIAlertView("Errore Dati", "E-Mail o Password Non Inseriti o Formato errato", null, "OK", null);
                adErr.Show();

            }
        }

        partial void PasswordLost(UIButton sender)
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("http://www.roccogiocattoli.eu/profilo/"));
        }

        partial void PasswordLostIpad(UIButton sender)
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("http://www.roccogiocattoli.eu/profilo/"));
        }

        public void LoginFB(string token)
        {
            Console.WriteLine("Token:" + token);

            UtilityLoginManager login = new UtilityLoginManager();
            List<string> result = new List<string>();
            result = login.LoginFB(token);

            if (result[0] == "SUCCESS")
            {
                Console.WriteLine(result[0] + "  " + result[1] + "  " + result[2] + "  " + result[3] + "  " + result[4]);

                NSUserDefaults.StandardUserDefaults.SetString(result[2], "NomeRoccoN4U");
                NSUserDefaults.StandardUserDefaults.SetString(result[3], "CognomeRoccoN4U");
                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }

                UIViewController lp;

                if (result[4] == "")
                {
                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "AppoggioTokenRoccoN4U");
                    lp = storyboard.InstantiateViewController("CardPage");
                }
                else
                {
                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "TokenRoccoN4U");
                    lp = storyboard.InstantiateViewController("RWController");
                }

                base.NavigationController.PresentModalViewController(lp, true);
            }

            if (result[0] == "Errore")
            {
                LoadView.Alpha = 0;
                LoadView.SetNeedsLayout();
                loginManager.LogOut();
                
                var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                adErr.Show();
            }
        }
    }
}
