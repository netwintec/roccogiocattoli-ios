// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RoccoGiocattoli
{
	[Register ("ProfiloViewController")]
	partial class ProfiloViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ContentView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView HeaderView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIBarButtonItem MenuItem { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView TitoloView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ContentView != null) {
				ContentView.Dispose ();
				ContentView = null;
			}
			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}
			if (MenuItem != null) {
				MenuItem.Dispose ();
				MenuItem = null;
			}
			if (TitoloView != null) {
				TitoloView.Dispose ();
				TitoloView = null;
			}
		}
	}
}
