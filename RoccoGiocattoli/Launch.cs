using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RoccoGiocattoli
{
    partial class Launch : UIViewController
    {
        public Launch(IntPtr handle) : base(handle)
        {
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));
        }
    }
        
}
