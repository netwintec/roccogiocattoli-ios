using CoreAnimation;
using CoreGraphics;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;

namespace RoccoGiocattoli
{
    partial class RegistratiPage : UIViewController
    {
        private LoginButton loginFbButton;
        private UIScrollView scrollView;
        UITextField Nome, Cognome, EMail, Password;
        List<string> readPermissions = new List<string> { "public_profile","email" };
        int yy = 0;
        nfloat ViewWidht;
        int SystemVersion;
        bool flagNome, flagCognome, flagEMail, flagPassword;

        public RegistratiPage(IntPtr handle) : base(handle)
        {
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ViewWidht = View.Frame.Width;
            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));

            LoadView.Alpha = 0;
            CABasicAnimation rotationAnimation = new CABasicAnimation();
            rotationAnimation.KeyPath = "transform.rotation.z";
            rotationAnimation.To = new NSNumber(Math.PI * 2);
            rotationAnimation.Duration = 2;
            rotationAnimation.Cumulative = true;
            rotationAnimation.RepeatCount = float.MaxValue;
            LoadingImage.Layer.AddAnimation(rotationAnimation, "rotationAnimation");

            Profile.Notifications.ObserveDidChange((sender, e) => {

                if (e.NewProfile == null)
                    return;

                //nameLabel.Text = e.NewProfile.Name;
            });

            // Set the Read and Publish permissions you want to get
            loginFbButton = new LoginButton(new CGRect(0, 0, 120, 40))//AccediText.Frame.X+(AccediText.Frame.Width/2) - 60, AccediText.Frame.Y, 120, 40))
            {
                LoginBehavior = LoginBehavior.Native,
                ReadPermissions = readPermissions.ToArray()
                
            };

            LoginManager loginManager = new LoginManager();


            // Handle actions once the user is logged in
            loginFbButton.Completed += (sender, e) => {
                if (e.Error != null)
                {
                    var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                    adErr.Show();
                    // Handle if there was an error
                }

                if (e.Result.IsCancelled)
                {
                    // Handle if the user cancelled the login request
                }

                if (e.Result.Token.TokenString != null)
                {
                    Console.WriteLine("Token:" + e.Result.Token.TokenString);

                    LoadView.Alpha = 1;
                    LoadView.SetNeedsLayout();

                    UtilityLoginManager login = new UtilityLoginManager();
                    List<string> result = new List<string>();
                    result = login.LoginFB(e.Result.Token.TokenString);

                    if (result[0] == "SUCCESS")
                    {
                        Console.WriteLine(result[0] + "  " + result[1] + "  " + result[2] + "  " + result[3] + "  " + result[4]);

                        NSUserDefaults.StandardUserDefaults.SetString(result[2], "NomeRoccoN4U");
                        NSUserDefaults.StandardUserDefaults.SetString(result[3], "CognomeRoccoN4U");
                        UIStoryboard storyboard = new UIStoryboard();
                        if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                        {
                            storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                        }
                        else
                        {
                            storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                        }

                        UIViewController lp;

                        if (result[4] == "")
                        {
                            NSUserDefaults.StandardUserDefaults.SetString(result[1], "AppoggioTokenRoccoN4U");
                            lp = storyboard.InstantiateViewController("CardPage");
                        }
                        else
                        {
                            NSUserDefaults.StandardUserDefaults.SetString(result[1], "TokenRoccoN4U");
                            lp = storyboard.InstantiateViewController("RWController");
                        }

                        base.NavigationController.PresentModalViewController(lp, true);
                    }

                    if (result[0] == "ERROR")
                    {
                        LoadView.Alpha = 0;
                        LoadView.SetNeedsLayout();
                        loginManager.LogOut();
                        var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                        adErr.Show();

                    }

                }

                // Handle your successful login
            };




            //RegisterButton.Layer.CornerRadius = 7;


            SystemVersion = Convert.ToInt16(UIDevice.CurrentDevice.SystemVersion.Split('.')[0]);
            Console.WriteLine("Version:"+SystemVersion+"   "+ UIDevice.CurrentDevice.SystemVersion);

            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height));


            UIImageView imageView = new UIImageView(new CGRect(ViewWidht / 2 - 100, yy + 14, 200, 86));
            imageView.Image=UIImage.FromFile("logo_rocco.png");
            yy += 100;

            UILabel Registrati = new UILabel(new CGRect(ViewWidht / 2 - 45, yy+20, 90, 25));
            Registrati.Text = "Registrati";
            Registrati.TextAlignment = UITextAlignment.Center;
            yy += 45;

            UIView FacebookView = new UIView(new CGRect(ViewWidht / 2 - 60, yy + 10, 120, 40));
            FacebookView.AddSubview(loginFbButton);
            yy += 50;

            UILabel Oppure = new UILabel(new CGRect(ViewWidht / 2 - 30, yy+10, 60, 25));
            Oppure.Text = "Oppure";
            Oppure.TextAlignment = UITextAlignment.Center;
            yy += 35;

            Nome = new UITextField(new CGRect(1, 1, ViewWidht-42, 30));
            Nome.KeyboardType = UIKeyboardType.Default;
            Nome.Text = "";
            Nome.Placeholder = "Nome";
            Nome.TextColor = UIColor.FromRGB(86,86,86);
            Nome.BackgroundColor = UIColor.FromRGB(211, 211, 211);
            Nome.BorderStyle = UITextBorderStyle.RoundedRect;
            Nome.ShouldReturn += (UITextField) => {
                flagNome = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView NomeView = new UIView(new CGRect(20, yy + 15, ViewWidht - 40, 32 ));
            NomeView.Layer.CornerRadius = 7;
            NomeView.BackgroundColor = UIColor.Black;
            NomeView.Add(Nome);

            yy += 47;

            Cognome = new UITextField(new CGRect(1, 1, ViewWidht - 42, 30));
            Cognome.KeyboardType = UIKeyboardType.Default;
            Cognome.Text = "";
            Cognome.Placeholder = "Cognome";
            Cognome.TextColor = UIColor.FromRGB(86, 86, 86);
            Cognome.BackgroundColor = UIColor.FromRGB(211, 211, 211);
            Cognome.BorderStyle = UITextBorderStyle.RoundedRect;
            Cognome.ShouldReturn += (UITextField) => {
                flagCognome = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView CognomeView = new UIView(new CGRect(20, yy + 5, ViewWidht - 40, 32));
            CognomeView.Layer.CornerRadius = 7;
            CognomeView.BackgroundColor = UIColor.Black;
            CognomeView.Add(Cognome);
            yy += 37;

            EMail = new UITextField(new CGRect(1, 1, ViewWidht - 42, 30));
            EMail.KeyboardType = UIKeyboardType.EmailAddress;
            EMail.Text = "";
            EMail.Placeholder = "E-Mail";
            EMail.TextColor = UIColor.FromRGB(86, 86, 86);
            EMail.BackgroundColor = UIColor.FromRGB(211, 211, 211);
            EMail.BorderStyle = UITextBorderStyle.RoundedRect;
            EMail.ShouldReturn += (UITextField) => {
                flagEMail = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView EmailView = new UIView(new CGRect(20, yy + 5, ViewWidht - 40, 32));
            EmailView.Layer.CornerRadius = 7;
            EmailView.BackgroundColor = UIColor.Black;
            EmailView.Add(EMail);
            yy += 37;

            Password = new UITextField(new CGRect(1, 1, ViewWidht - 42, 30));
            Password.KeyboardType = UIKeyboardType.Default;
            Password.SecureTextEntry = true;
            Password.Text = "";
            Password.Placeholder = "Password";
            Password.TextColor = UIColor.FromRGB(86, 86, 86);
            Password.BackgroundColor = UIColor.FromRGB(211, 211, 211);
            Password.BorderStyle = UITextBorderStyle.RoundedRect;
            Password.ShouldReturn += (UITextField) => {
                flagPassword = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView PasswordView = new UIView(new CGRect(20, yy + 5, ViewWidht - 40, 32));
            PasswordView.Layer.CornerRadius = 7;
            PasswordView.BackgroundColor = UIColor.Black;
            PasswordView.Add(Password);
            yy += 37;

            //RegisterButton.Frame = ;
            UIButton BottoneRegistrati = new UIButton(new CGRect(ViewWidht / 2 - 110, yy + 30, 220, 40));
            BottoneRegistrati.SetTitle("Registrati", UIControlState.Normal) ;
            BottoneRegistrati.BackgroundColor = UIColor.FromRGB(233,69,141);
            BottoneRegistrati.Layer.CornerRadius = 7;
            BottoneRegistrati.TouchUpInside += BottoneRegistrati_TouchUpInside;


            yy += 70;

            UILabel Info = new UILabel(new CGRect(ViewWidht / 2 - 110, yy + 2, 220, 28));
            Info.Text = "Cliccando sul bottone qua sopra si accettano i termini di utilizzo di questa app";
            Info.TextAlignment = UITextAlignment.Center;
            Info.Font = UIFont.SystemFontOfSize(10);
            Info.Lines = 2;
            yy += 30;



            scrollView.Add(imageView);
            scrollView.Add(Registrati);
            scrollView.Add(FacebookView);
            scrollView.Add(Oppure);
            scrollView.Add(NomeView);
            scrollView.Add(CognomeView);
            scrollView.Add(EmailView);
            scrollView.Add(PasswordView);
            scrollView.Add(BottoneRegistrati);
            scrollView.Add(Info);

            scrollView.ContentSize = new CGSize(View.Frame.Width,yy+10);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            Content.Add(scrollView);


            Nome.ShouldBeginEditing = delegate {
                flagNome = true;
                CGRect frame = View.Frame;
                Console.Out.WriteLine("Dimensioni:"+UIScreen.MainScreen.Bounds);
                if (View.Frame.Height == 480)
                {
                    frame.Y = -115;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {

                    frame.Y = -26;
                    View.Frame = frame;
                }

                return true;
            };

            Cognome.ShouldBeginEditing = delegate {
                flagCognome = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -155;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -61;
                    View.Frame = frame;
                }

                return true;
            };

            EMail.ShouldBeginEditing = delegate {
                flagEMail = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -195;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -106;
                    View.Frame = frame;
                }

                if (View.Frame.Height == 667)
                {
                    frame.Y = -30;
                    View.Frame = frame;
                }

                return true;
            };

            Password.ShouldBeginEditing = delegate {
                flagPassword = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -205;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -116;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 667)
                {
                    frame.Y = -40;
                    View.Frame = frame;
                }

                return true;
            };

            Nome.ShouldEndEditing = delegate {
                
                CGRect frame = View.Frame;
                frame.Y = 0;
                if(flagNome == false)
                    View.Frame = frame;
                return true;
            };

            Cognome.ShouldEndEditing = delegate {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagCognome == false)
                    View.Frame = frame;
                return true;
            };

            EMail.ShouldEndEditing = delegate {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagEMail == false)
                    View.Frame = frame;
                return true;
            };

            Password.ShouldEndEditing = delegate {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagPassword == false)
                    View.Frame = frame;
                return true;
            };
        }

        private void BottoneRegistrati_TouchUpInside(object sender, EventArgs e)
        {
            
            int errore = 0;
            UtilityLoginManager register = new UtilityLoginManager();
            List<string> result = new List<string>();
            if (Nome == null && Cognome == null && EMail == null && Password==null)
            {
                result.Add("ERRORNULL");
            }
            else
            {
                LoadView.Alpha = 1;
                LoadView.SetNeedsLayout();
                result = register.ValidationReg(Nome.Text, Cognome.Text, EMail.Text, Password.Text);
            }

            if (result[0] == "SUCCESS")
            {
                Console.WriteLine(result[0] + "  " + result[1]);
                //NSUserDefaults.StandardUserDefaults.SetString(result[1], "AppoggioTokenRoccoN4U");
                NSUserDefaults.StandardUserDefaults.SetString(Nome.Text, "NomeRoccoN4U");
                NSUserDefaults.StandardUserDefaults.SetString(Cognome.Text, "CognomeRoccoN4U");
                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }

                UIViewController lp;

                if (result[2] == "")
                {
                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "AppoggioTokenRoccoN4U");
                    lp = storyboard.InstantiateViewController("CardPage");
                }
                else
                {
                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "TokenRoccoN4U");
                    lp = storyboard.InstantiateViewController("RWController");
                }

                base.NavigationController.PresentViewController(lp, true, null);
            }

            if (result[0] == "ERROR")
            {
                LoadView.Alpha = 0;
                LoadView.SetNeedsLayout();

                Nome.Text = "";
                Cognome.Text = "";
                EMail.Text = "";
                Password.Text = "";
                var adErr = new UIAlertView("Registrazione Non Riuscita", "Errore di rete", null, "OK", null);
                adErr.Show();

            }

            if (result[0] == "ERROREXIST")
            {
                LoadView.Alpha = 0;
                LoadView.SetNeedsLayout();

                Nome.Text = "";
                Cognome.Text = "";
                EMail.Text = "";
                Password.Text = "";
                var adErr = new UIAlertView("Registrazione Non Riuscita", "Email gi� esistente", null, "OK", null);
                adErr.Show();

            }


            if (result[0] == "ERRORDATA")
            {
                LoadView.Alpha = 0;
                LoadView.SetNeedsLayout();

                Console.WriteLine(result[1]);
                if (result[1].Contains("N"))
                    errore++;
                if (result[1].Contains("C"))
                    errore++;
                if (result[1].Contains("P"))
                    errore++;
                if (result[1].Contains("E"))
                {
                    errore++;
                    EMail.Text = "";
                }
                if (errore == 1)
                {
                    var adErr = new UIAlertView("Errore", "Errore 1 campo da lei inserito contiene valori errati o non contiene nessun valore, perfavore controlli", null, "OK", null);
                    adErr.Show();
                }
                if (errore >= 2)
                {
                    var adErr = new UIAlertView("Errore", "Errore 2 o pi� campi da lei inseriti contengono valori errati o non contengono nessun valore, perfavore controlli", null, "OK", null);
                    adErr.Show();
                }


            }

            if (result[0] == "ERRORNULL")
            {

                var adErr = new UIAlertView("Errore", "Alcuni campi da lei inseriti contengono valori errati o non contengono nessun valore, perfavore controlli", null, "OK", null);
                adErr.Show();
                if (Nome != null) Nome.Text = "";
                if (Cognome != null) Cognome.Text = "";
                if (Password != null) Password.Text = "";
                if (EMail != null) EMail.Text = "";
            }
        }
    }
}
