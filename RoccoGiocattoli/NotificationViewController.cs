using CoreGraphics;
using Foundation;
using RestSharp;
using System;
using System.CodeDom.Compiler;
using System.Text;
using System.Threading;
using UIKit;

namespace RoccoGiocattoli
{
	partial class NotificationViewController : UIViewController
	{

        string token = "650eb4ad-7b45-4bca-9783-138ff9115e88";
        int porta = 81;
        bool isPhone;
        double Px, Py, Height, Widht;
        int yy;
        UIScrollView scrollView;
        nfloat ViewWidht;

		public NotificationViewController (IntPtr handle) : base (handle)
		{
            //650eb4ad - 7b45 - 4bca - 9783 - 138ff9115e88
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewWidht = View.Frame.Width;
            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));

            yy = 0;
            //NSUserDefaults.StandardUserDefaults.SetString(token, "TokenRoccoN4U");

            bool isPushNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isPushNotification");
            bool isBeaconNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isBeaconNotification");

            if (isPushNotification != null && isPushNotification == true)
            {
                // Viene dal push
                var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                //var requestN4U = new RestRequest("login/facebook?access_token=" + TokenFB, Method.GET);
                var requestN4U = new RestRequest("notification/push/"+ NSUserDefaults.StandardUserDefaults.StringForKey("KeyNotification"), Method.GET);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");

                Console.WriteLine("notification/push/" + NSUserDefaults.StandardUserDefaults.StringForKey("KeyNotification"));

                try {
                    client.Execute(requestN4U);
                }
                catch (Exception e)
                {
                    Console.WriteLine("push");
                }
                NSUserDefaults.StandardUserDefaults.SetBool(false, "isPushNotification");
            }

            if (isBeaconNotification != null && isBeaconNotification == true)
            {
                // Viene dal beacon
                var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                //var requestN4U = new RestRequest("login/facebook?access_token=" + TokenFB, Method.GET);
                var requestN4U = new RestRequest("notification/beacon/" + NSUserDefaults.StandardUserDefaults.StringForKey("KeyNotification"), Method.GET);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
                requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));

                try {
                    client.Execute(requestN4U);
                }
                catch (Exception e)
                {
                    Console.WriteLine("beacon");
                }
                NSUserDefaults.StandardUserDefaults.SetBool(false, "isBeaconNotification");
            }


            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height));
            scrollView.BackgroundColor = UIColor.Clear;
            ContentView.BackgroundColor = UIColor.Clear;

            UIStoryboard storyboard = new UIStoryboard();

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                isPhone = true;
                Widht = (View.Frame.Width - 60) / 2;
                Height = Widht;
                Px = 20;
                Py = 20;
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                isPhone = false;
                Widht = (View.Frame.Width - 240) / 2;
                Height = Widht;
                Px = 80;
                Py = 80;
            }

            UIImageView imageView = new UIImageView(new CGRect(ViewWidht / 2 - 50, 20, 100, 100));
            imageView.Image = UIImage.FromFile("placeholder.png"); 
            


            yy += 120;

            UILabel Label = new UILabel() ;
            if (isPhone)
            {

                var size = UIStringDrawing.StringSize(NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotification"),Label.Font, new CGSize(200, 2000));
                Console.WriteLine("4:" + size.Height + "  " + size.Width);

                if (NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotification").CompareTo("null") == 0) {

                    Label = new UILabel(new CGRect(ViewWidht / 2 - 100, 20, 200, size.Height));
                    yy -= 120;
                }
                else
                {
                    Label = new UILabel(new CGRect(ViewWidht / 2 - 100, yy + 20, 200, size.Height));
                }
                Label.Text = NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotification");
                //Label.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Label.TextAlignment = UITextAlignment.Center;
                Label.Lines = 0;
                yy += (int)(size.Height+20);
            }
            else
            {

                var size = UIStringDrawing.StringSize(NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotification"), Label.Font, new CGSize(400, 2000));
                Console.WriteLine("4:" + size.Height + "  " + size.Width);

                if (NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotification").CompareTo("null") == 0)
                {

                    Label = new UILabel(new CGRect(ViewWidht / 2 - 100, 20, 400, size.Height));
                    yy -= 100;
                }
                else {
                    Label = new UILabel(new CGRect(ViewWidht / 2 - 200, yy + 20, 400, size.Height));
                }
                Label.Text = NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotification");
                //Label.Font = UIFont.FromName("BubblegumSans-Regular", 20);
                Label.TextAlignment = UITextAlignment.Center;
                Label.Lines = 0;
                yy += (int)(size.Height + 20);
            }

            
            
            yy += 23;

            Console.WriteLine("Image:"+NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotification")+"|"+ NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotification").CompareTo("null"));

            if(NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotification").CompareTo("null")!=0)
                scrollView.Add(imageView);

            scrollView.Add(Label);


            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 20);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            ContentView.Add(scrollView);

            if (NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotification").CompareTo("null") != 0)
                SetImageAsync(imageView,Label);
        }


        public void SetImageAsync(UIImageView image, UILabel label)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotification")) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        Console.WriteLine(w + "  " + h);
                        image.Frame = new CGRect(20, 20, View.Frame.Width - 40, ((View.Frame.Width - 40) * h) / w);
                        Double yyy = ((View.Frame.Width - 40) * h) / w + 20;


                        if (isPhone)
                        {
                            var size = UIStringDrawing.StringSize(NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotification"), label.Font, new CGSize(200, 2000));
                            Console.WriteLine("4:" + size.Height + "  " + size.Width);

                            label.Frame = new CGRect(ViewWidht / 2 - 100, yyy + 20, 200, size.Height);
                            yyy += (int)size.Height+20;
                        }
                        else
                        {
                            var size = UIStringDrawing.StringSize(NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotification"), label.Font, new CGSize(400, 2000));
                            Console.WriteLine("4:" + size.Height + "  " + size.Width);

                            label.Frame = new CGRect(ViewWidht / 2 - 200, yyy + 20, 400, size.Height);
                            yyy += (int)size.Height + 20;
                        }

                        scrollView.ContentSize = new CGSize(View.Frame.Width, yyy + 20);
                        image.Image = cachedImage;

                    });

                });
            }catch(Exception e)
            {
                Console.WriteLine("URL LOAD :"+e.StackTrace);
            }
   
        }


        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }
    }
}
