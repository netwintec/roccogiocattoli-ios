﻿using System;
using System.Drawing;
using Foundation;
using UIKit;
using System.Collections.Generic;
using System.Text;
using RestSharp;

namespace RoccoGiocattoli
{
    public partial class RootViewController : UIViewController
    {
        public Dictionary<int, XmlObject> XmlObjectDictionary = new Dictionary<int, XmlObject>();
        public int totalValue;
		int porta = 81;

		public string PushToken;

        public string TokenN4U="";
        public bool flagTokenN4U=true;
        public bool Scan = false;

        public RootViewController(IntPtr handle) : base(handle)
        {
            Console.WriteLine("costr");
        }

        public static RootViewController Self { get; private set; }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad()
        {
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            UIApplication.SharedApplication.CancelAllLocalNotifications();
            

            base.ViewDidLoad();

            Console.WriteLine("didload "+View.Frame.Height);

            //NSUserDefaults.StandardUserDefaults.SetString("", "TokenRoccoN4U");

            bool isPushNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isPushNotification");
            bool isBeaconNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isBeaconNotification");
            Console.WriteLine(isPushNotification+"|"+isBeaconNotification);

            if (isPushNotification == true)
            {
                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }
                UIViewController lp = storyboard.InstantiateViewController("Notification");
                base.NavigationController.PresentModalViewController(lp, true);
            }

            if (isBeaconNotification == true)
            {
                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                }
                UIViewController lp = storyboard.InstantiateViewController("Notification");
                base.NavigationController.PresentModalViewController(lp, true);
            }

            if (isPushNotification == false && isBeaconNotification == false)
            {
                if (NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U") != null && NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U") != "")
                {
                    var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                    Console.WriteLine(NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));

                    var requestN4U = new RestRequest("profile", Method.GET);
                    requestN4U.AddHeader("content-type", "application/json");
                    requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
                    requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U"));  // "e74e8d8c-b4c5-482b-9f0e-d5fe2f657c1b"
                    requestN4U.Timeout = 600000;


                    IRestResponse response = client.Execute(requestN4U);
                    Console.WriteLine("RESPONSE CODE:" + response.StatusCode);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        UIStoryboard storyboard = new UIStoryboard();
                        if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                        {
                            storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                        }
                        else
                        {
                            storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                        }
                        UIViewController lp = storyboard.InstantiateViewController("RWController");
                        base.NavigationController.PresentModalViewController(lp, true);
                        image.Alpha = 0;
                        imageSplash.Alpha = 1;
                        AccediButton.Alpha = 0;
                        RegistratiButton.Alpha = 0;
                        NavigationController.SetNavigationBarHidden(true, false);
                        //TokenN4U = NSUserDefaults.StandardUserDefaults.StringForKey("TokenRoccoN4U");
                    }
                    else
                    {
                        base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));
                    }

                }
                else
                {
                    base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));
                }
            }

            AccediButton.Layer.CornerRadius = 7;
            RegistratiButton.Layer.CornerRadius = 7;

            


            /*
            var fontList = new StringBuilder();
            var familyNames = UIFont.FamilyNames;

            foreach (var familyName in familyNames)
            {
                fontList.Append(String.Format("Family; {0} \n", familyName));
                Console.WriteLine("Family: {0}\n", familyName);

                var fontNames = UIFont.FontNamesForFamilyName(familyName);

                foreach (var fontName in fontNames)
                {
                    Console.WriteLine("\tFont: {0}\n", fontName);
                }

                // Perform any additional setup after loading the view, typically from a nib.
            }*/
        }

        public override void ViewWillAppear(bool animated)
        {
            Console.WriteLine("appear");
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            Console.WriteLine("didappear");
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}