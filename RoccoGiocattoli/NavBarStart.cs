using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RoccoGiocattoli
{
	partial class NavBarStart : UINavigationBar
	{
		public NavBarStart (IntPtr handle) : base (handle)
		{
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            //Console.WriteLine(Frame.Width);

            UIImageView centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 40, 4, 80, 36));
            centerlogo.Image = UIImage.FromFile("logo_rocco.png");

            AddSubview(centerlogo);
        }
    }
}
