
using System;
using System.Drawing;

using Foundation;
using UIKit;

namespace RoccoGiocattoli
{
    public partial class LaunchScreen : UIViewController
    {
        static bool UserInterfaceIdiomIsPhone
        {
            get {
                Console.WriteLine("1");
                return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        public LaunchScreen(IntPtr handle) : base(handle)
        {
            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
            Console.WriteLine("2");
            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Console.WriteLine("3");
            base.View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("pallini-sfondo.jpg"));
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void ViewWillAppear(bool animated)
        {
            Console.WriteLine("4");
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            Console.WriteLine("5");
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            Console.WriteLine("6");
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            Console.WriteLine("7");
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}