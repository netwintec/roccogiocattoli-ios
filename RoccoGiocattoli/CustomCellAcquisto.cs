﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace RoccoGiocattoli
{
    public class CustomCellAcquisto : UITableViewCell
    {
        UILabel nomeLabel, prezzoLabel;
        UIImageView imageView;
        UIView separator;
        bool isPhone;
        public CustomCellAcquisto(NSString cellId,bool phone) : base(UITableViewCellStyle.Default, cellId)
        {
            isPhone = phone;
            SelectionStyle = UITableViewCellSelectionStyle.None;
            //ContentView.BackgroundColor = UIColor.FromRGB(218, 255, 127);
            imageView = new UIImageView();
            separator = new UIView();
            separator.BackgroundColor = UIColor.FromRGB(0,160,210);
            nomeLabel = new UILabel()
            {
                //Font = UIFont.FromName("", 22f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                //TextColor = UIColor.FromRGB(127, 51, 0),
                BackgroundColor = UIColor.Clear
            };
            prezzoLabel = new UILabel()
            {
                //Font = UIFont.FromName("AmericanTypewriter", 12f),
                //TextColor = UIColor.FromRGB(38, 127, 0),
                //Font = UIFont.SystemFontOfSize(14),
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear
            };

            if (isPhone)
            {
                nomeLabel.Font = UIFont.BoldSystemFontOfSize(16);
                prezzoLabel.Font = UIFont.SystemFontOfSize(14);
            }
            else
            {
                nomeLabel.Font = UIFont.BoldSystemFontOfSize(24);
                prezzoLabel.Font = UIFont.SystemFontOfSize(22);
            }
            ContentView.AddSubviews(new UIView[] { nomeLabel, prezzoLabel, imageView,separator });

        }
        public void UpdateCell(string nome, string prezzo, UIImage image)
        {
            imageView.Image = image;
            nomeLabel.Text = nome;
            prezzoLabel.Text = prezzo + "€";
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            if (isPhone)
            {
                nfloat widht = (ContentView.Frame.Width - 40) / 12;
                nfloat height = (widht * 27) / 35;
                nomeLabel.Frame = new CGRect(15, (ContentView.Frame.Height / 2) - 12.5, widht * 8, 25);
                prezzoLabel.Frame = new CGRect(20 + (8 * widht), (ContentView.Frame.Height / 2) - 12.5, widht * 3, 25);
                imageView.Frame = new CGRect(25 + (11 * widht), (ContentView.Frame.Height / 2) - (height / 2), widht, height);
            }
            else
            {
                nfloat widht = (ContentView.Frame.Width - 40) / 18;
                nfloat height = (widht * 27) / 35;
                nomeLabel.Frame = new CGRect(15, (ContentView.Frame.Height / 2) - 15, widht * 12, 30);
                prezzoLabel.Frame = new CGRect(20 + (12 * widht), (ContentView.Frame.Height / 2) - 15, widht * 5, 30);
                imageView.Frame = new CGRect(25 + (17 * widht), (ContentView.Frame.Height / 2) - (height / 2), widht, height);
            }

            separator.Frame = new CGRect(10, ContentView.Frame.Height-2, ContentView.Frame.Width-20, 2);
        }
    }
}
