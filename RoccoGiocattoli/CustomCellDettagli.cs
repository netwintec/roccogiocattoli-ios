﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace RoccoGiocattoli
{
    public class CustomCellDettagli : UITableViewCell
    {
        UILabel dataLabel, PVLabel;
        bool isPhone;
        public CustomCellDettagli(NSString cellId, bool phone) : base(UITableViewCellStyle.Default, cellId)
        {
            isPhone = phone;

            SelectionStyle = UITableViewCellSelectionStyle.None;
            dataLabel = new UILabel()
            {
                //Font = UIFont.FromName("", 22f),
                
                //TextColor = UIColor.FromRGB(127, 51, 0),
                BackgroundColor = UIColor.Clear
            };
            
            PVLabel = new UILabel()
            {
                //Font = UIFont.FromName("AmericanTypewriter", 12f),
                //TextColor = UIColor.FromRGB(38, 127, 0),
                
                //TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear
            };

            if (isPhone)
            {
                dataLabel.Font = UIFont.SystemFontOfSize(14);
                PVLabel.Font = UIFont.SystemFontOfSize(14);
            }
            else
            {
                dataLabel.Font = UIFont.SystemFontOfSize(22);
                PVLabel.Font = UIFont.SystemFontOfSize(22);
            }
            ContentView.AddSubviews(new UIView[] { dataLabel, PVLabel });

        }
        public void UpdateCell(string data, string PV)
        {
            dataLabel.Text = data;
            PVLabel.Text = PV;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            if (isPhone)
            {
                dataLabel.Frame = new CGRect(15, 7, 200, 25);
                PVLabel.Frame = new CGRect(15, 38, 200, 25);
            }
            else
            {
                dataLabel.Frame = new CGRect(15, 11, 400, 35);
                PVLabel.Frame = new CGRect(15, 54, 400, 35);
            }
        }
    }
}

