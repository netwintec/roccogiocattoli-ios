using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RoccoGiocattoli
{
	partial class TableRow : UITableViewCell
	{
		public TableRow (IntPtr handle) : base (handle)
		{
		}
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            base.BackgroundColor = UIColor.FromRGB(83, 83, 83);

            Label.Font = UIFont.FromName("BubblegumSans-Regular", 20);

        }
    }
}
